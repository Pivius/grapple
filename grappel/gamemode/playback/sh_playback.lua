if SERVER then
  util.AddNetworkString( "PBSync" )
end
Playback = {}
Playback.Players = {}
Playback.MaxPlayers = 16
Playback.MaxLength = 120 //2 min
Playback.MaxBots = 1
Playback.Bots = {}

function SpawnBots( )

	for _,bot in pairs( player.GetBots() ) do
    if !bot.Exist then
			bot:SetMoveType( MOVETYPE_NONE )
			bot:SetCollisionGroup( COLLISION_GROUP_DEBRIS )
			bot.Player = "None"

			bot:SetFOV( 100, 0 )
			bot:SetGravity( 0 )
      bot.Exist = true
      local t = {
        [bot] = {
          --["Replaying"] = false,
          ["PlayFrame"] = 0,
          ["Occupied"] = false,
          ["Data"] = {
            [1] = {},
            [2] = {},
            [3] = {},
            [4] = {},
            [5] = {},
            [6] = {}
          },
          ["Frames"] = 0
        }
      }
      table.Merge(Playback.Bots, t)
    end
	end

	if #player.GetBots() < Playback.MaxBots then

		RunConsoleCommand( "bot" )
		timer.Simple( 1, function()
			SpawnBots( )
		end )
	end
end

SpawnBots( )

function Playback:AddPlayer(ply)
  local startTime = CurTime()
  local t = {
    [ply] = {
      ["Frames"] = 0,
      ["Data"] = {
        [1] = {},
        [2] = {},
        [3] = {},
        [4] = {},
        [5] = {},
        [6] = {}
      },
      ["StartTime"] = startTime,
      ["Recording"] = true,
      ["Replaying"] = false,
      ["PlayFrame"] = 0
    }
  }
  table.Merge(Playback.Players, t)
	print( "Recording" )
end

function Playback:RemovePlayer(ply)
  Playback.Players[ply]["Recording"] = false
  print( "Recording ended" )
end

function Playback:Play(ply)
  if Playback.Players[ply]["Recording"] then return end

  --Playback.Players[ply]["Replaying"] = true
  --Playback.Players[ply]["PlayFrame"] = 0
  local ReplayBot = nil
  for bot, val in pairs(Playback.Bots) do
    if !bot["Occupied"] then
      Playback.Bots[bot]["Occupied"] = true
      Playback.Bots[bot]["Data"] = Playback.Players[ply]["Data"]
      Playback.Bots[bot]["Frames"] = Playback.Players[ply]["Frames"]
      Playback.Bots[bot]["PlayFrame"] = 0
      ReplayBot = bot
      break
    end
  end
  if ReplayBot then
    Spectate.StartSpec(ply, ReplayBot)
  end
  print( "Playback" )
end

function Playback:Stop(ply)
  if Playback.Players[ply]["Recording"] then return end
  --Playback.Players[ply]["Replaying"] = false
  --Playback.Players[ply]["PlayFrame"] = 0
  Spectate.EndSpec(ply)
  print( "Playback ended" )
end

local function Record(ply, mv)
  if ply:IsValid() and UT:HasKey( Playback.Players, ply ) and Playback.Players[ply]["Recording"] then
    if Playback.Players[ply]["StartTime"]+Playback.MaxLength < CurTime() then

       Playback:RemovePlayer(ply)
      return
    end
    local origin = mv:GetOrigin()
		local angles = mv:GetAngles()
		local frame = Playback.Players[ply]["Frames"]

    Playback.Players[ply]["Data"][1][frame] = origin.x
    Playback.Players[ply]["Data"][2][frame] = origin.y
    Playback.Players[ply]["Data"][3][frame] = origin.z
    Playback.Players[ply]["Data"][4][frame] = angles.p
    Playback.Players[ply]["Data"][5][frame] = angles.y


    Playback.Players[ply]["Frames"] = frame+1

  end

end
hook.Add( "SetupMove", "PlaybackRecord", Record )

local function ButtonRecord( ply, cmd )
	 if ply:IsValid() and UT:HasKey( Playback.Players, ply ) and Playback.Players[ply]["Recording"] then
    local frame = Playback.Players[ply]["Frames"]
    Playback.Players[ply]["Data"][6][frame] = cmd:GetButtons()
  end
end
hook.Add( "StartCommand", "ButtonRecord", ButtonRecord )

local function Replay(ply, mv)
  if ply:IsBot() and UT:HasKey( Playback.Bots, ply ) and Playback.Bots[ply]["Frames"] > 0 then

    local frame = Playback.Bots[ply]["PlayFrame"]
    if frame >= Playback.Bots[ply]["Frames"] then
      local data = Playback.Bots[ply]["Data"]
      Playback.Bots[ply]["Occupied"] = false
      Playback.Bots[ply]["PlayFrame"] = 0
      mv:SetOrigin( Vector( data[ 1 ][ 0 ], data[ 2 ][ 0 ], data[ 3 ][ 0 ] ) )
      return ply:SetEyeAngles( Angle( data[ 4 ][ 0 ], data[ 5 ][ 0 ], 0 ) )
    else
      local data = Playback.Bots[ply]["Data"]
      mv:SetOrigin( Vector( data[ 1 ][ frame ], data[ 2 ][ frame ], data[ 3 ][ frame ] ) )
      ply:SetEyeAngles( Angle( data[ 4 ][ frame ], data[ 5 ][ frame ], 0 ) )
      Playback.Bots[ply]["PlayFrame"] = frame + 1
    end
  end
end
hook.Add( "SetupMove", "Replay", Replay )

local function ButtonReplay( ply, cmd )
  if ply:IsBot() and UT:HasKey( Playback.Bots, ply ) and Playback.Bots[ply]["Frames"] > 0 then
		cmd:ClearButtons()
		cmd:ClearMovement()
    local data = Playback.Bots[ply]["Data"]
    local frame = Playback.Bots[ply]["PlayFrame"]
		if data[6][ frame ] and ply:GetMoveType() == 0 then
      ply:SetEyeAngles( Angle( data[ 4 ][ frame ], data[ 5 ][ frame ], 0 ) )
			cmd:SetButtons( tonumber( data[ 6 ][ frame ] ) )
		end
	end
end
hook.Add( "StartCommand", "ButtonReplay", ButtonReplay )

/*
local function Replay(ply, mv)
  if ply:IsValid() and UT:HasKey( Playback.Players, ply ) and Playback.Players[ply]["Replaying"] then

    local frame = Playback.Players[ply]["PlayFrame"]
    if frame >= Playback.Players[ply]["Frames"] then
      local data = Playback.Players[ply]["Data"]
      Playback:Stop(ply)
      mv:SetOrigin(Vector( data[ 1 ][ 0 ], data[ 2 ][ 0 ], data[ 3 ][ 0 ] ))
    else
      local data = Playback.Players[ply]["Data"]
      mv:SetOrigin( Vector( data[ 1 ][ frame ], data[ 2 ][ frame ], data[ 3 ][ frame ] ) )
      ply:SetEyeAngles( Angle( data[ 4 ][ frame ], data[ 5 ][ frame ], 0 ) )
      Playback.Players[ply]["PlayFrame"] = frame + 1
    end
  end
end
hook.Add( "SetupMove", "Replay", Replay )

local function ButtonReplay( ply, cmd )
  if ply:IsValid() and UT:HasKey( Playback.Players, ply ) and Playback.Players[ply]["Replaying"] then
		cmd:ClearButtons()
		cmd:ClearMovement()
    local data = Playback.Players[ply]["Data"]
    local frame = Playback.Players[ply]["PlayFrame"]
		if data[6][ frame ] and ply:GetMoveType() == 0 then
			cmd:SetButtons( tonumber( data[ 6 ][ frame ] ) )
		end
	end
end
hook.Add( "StartCommand", "ButtonReplay", ButtonReplay )
*/
concommand.Add( "StartRecord", function( ply, cmd, args )
	--Playback:AddPlayer(ply)
  net.Start( "PBSync" )
    net.WriteString("AddPlayer")
    net.WriteEntity(ply)
  net.SendToServer()

end )

concommand.Add( "EndRecord", function( ply, cmd, args )
	--Playback:RemovePlayer(ply)
  net.Start( "PBSync" )
    net.WriteString("RemovePlayer")
    net.WriteEntity(ply)
  net.SendToServer()

end )

concommand.Add( "Playback", function( ply, cmd, args )
	--Playback:Play(ply)
  net.Start( "PBSync" )
    net.WriteString("Play")
    net.WriteEntity(ply)
  net.SendToServer()

end )

concommand.Add( "EndPlayback", function( ply, cmd, args )
	--Playback:Stop(ply)
  net.Start( "PBSync" )
    net.WriteString("End")
    net.WriteEntity(ply)
  net.SendToServer()

end )

net.Receive( "PBSync", function( len, ply )
	 local str = net.ReadString()
   if str == "AddPlayer" then
     Playback:AddPlayer(net.ReadEntity())
   elseif str == "RemovePlayer" then
     Playback:RemovePlayer(net.ReadEntity())
   elseif str == "Play" then
     Playback:Play(net.ReadEntity())
   elseif str == "End" then
     Playback:Stop(net.ReadEntity())
   end
end )
