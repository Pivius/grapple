lobby = lobby or {}
lobby.lobbies = {}
lobby.keepopen = false

--include("modules/cl_hudedit.lua")

function syncLobbies()
  net.Start("lobbyNetworking")
    net.WriteString("sync")
    net.WriteTable({LocalPlayer()})
  net.SendToServer()
end

local BUTTON =
{

  Init = function( self )
    self.Font = "HUD Menu"
    self.Text = ""

    self.Color = Color(255,255,255, 50)
    self.Highlight = Color(0,0,0)
    local alpha = 0

    self.ButtonColor = self:Add( "DModelPanel" )

    self.ButtonColor:SetAlpha(255)
    self.ButtonColor:SetColor(self.Color)
    self.ButtonColor:SetSize( 0, 0 )

    self.Button = self:Add( "DButton" )
    self.Button.Font = self.Font
    self.Alpha = vgui.Create("DButton")
    self.Alpha:SetSize(0,0)
    self.Alpha:SetAlpha(self.Color.a)

    self.Alpha:SetText("")
    self.Alpha.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, 255) )
      surface.DrawRect(0, 0, w, h)
    end

    self.Button:SetText("")
    local alphabut = self.Alpha
    local colorbut = self.ButtonColor
    self.Button:SetSize(self:GetWide(),self:GetTall())
    --self.Button:Dock(TOP)
    self.Button.OnClick = function()
    end
    self.Button.DoClick = function()
      self.Button.OnClick()
      alphabut:SetAlpha(250)
      alphabut:AlphaTo( alpha, 0.2, 0)
      colorbut:SetColor(self.Highlight)
      colorbut:ColorTo(self.Color, 0.2)
    end
    self.Button:SetAlpha(255)
    self.Button.OnCursorEntered = function(self)
      alpha = self:GetParent().Color.a+50
      alphabut:AlphaTo( alpha, 0.3 , 0)
    end

    self.Button.OnCursorExited = function(self)
      alpha = self:GetParent().Color.a
      alphabut:AlphaTo( alpha, 0.3 , 0)
    end

    self.Button.Paint = function(self, w, h)

      surface.SetDrawColor( Color(colorbut:GetColor().r,colorbut:GetColor().g,colorbut:GetColor().b, alphabut:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( self:GetParent().Font )
      self.txtw, self.txth = surface.GetTextSize(self:GetParent().Text)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self:GetParent().Text )
    end

	end,
  Update = function(self)
    if !self.Button then return end
    self.ButtonColor:SetColor(self.Color)

    self.Button.Font = self.Font
    self.Alpha:SetAlpha(self.Color.a)
    self.Button:SetText("")
    self.Button:SetSize(self:GetWide(),self:GetTall())
    /*
      local alphabut = self.Alpha
    local colorbut = self.ButtonColor
    self.Button.Paint = function(self, w, h)

      surface.SetDrawColor( Color(colorbut:GetColor().r,colorbut:GetColor().g,colorbut:GetColor().b, alphabut:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( self:GetParent().Font )
      self.txtw, self.txth = surface.GetTextSize(self:GetParent().Text)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self:GetParent().Text )
    end
    */
  end,
  SetFont = function(self, font)
    self.Font = font
    self:Update()
  end,

  SetText = function(self, txt)
    self.Text = txt
    self:Update()
  end,

  SetColor = function(self, col)
    self.Color = col
    self:Update()
  end,

  SetHighlight = function(self, h)
    self.Highlight = h
    self:Update()
  end,



	Paint = function( self, w, h )
	end,

  Think = function( self, w, h )
  end
}

BUTTON = vgui.RegisterTable( BUTTON, "EditablePanel" )

local TEXTEDIT =
{

  Init = function( self )
    self.Font = "HUD Menu"
    self.Text = ""
    self.DefText = ""
    self.Numeric = false
    self.ClickText = ""
    self.OnType = function() end
    self.OnEnter = function() end
    self.Color = Color(255,255,255, 50)
    self.Highlight = Color(0,0,0)
    local alpha = 0

    self.ButtonColor = self:Add( "DModelPanel" )

    self.ButtonColor:SetAlpha(255)
    self.ButtonColor:SetColor(self.Color)
    self.ButtonColor:SetSize( 0, 0 )

    self.Button = self:Add( "DButton" )
    self.Button.Font = self.Font
    self.Alpha = vgui.Create("DButton")
    self.Alpha:SetSize(0,0)
    self.Alpha:SetAlpha(self.Color.a)

    self.Alpha:SetText("")
    self.Alpha.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, 255) )
      surface.DrawRect(0, 0, w, h)
    end

    self.Button:SetText("")
    local alphabut = self.Alpha
    local colorbut = self.ButtonColor
    self.Button:SetSize(self:GetWide(),self:GetTall())
    --self.Button:Dock(TOP)
    self.Button.OnClick = function()
    end
    self.Button.DoClick = function()
      self.Button.OnClick()
      alphabut:SetAlpha(250)
      alphabut:AlphaTo( alpha, 0.2, 0)
      colorbut:SetColor(self.Highlight)
      colorbut:ColorTo(self.Color, 0.2)

      if not self.editing then
        self.editing = true
        self.txt = self.txt or {}
        self.txt = vgui.Create( "DTextEntry" )

        self.txt:SetSize( 0, 0 )
        self.txt:SetPos(0, 0)
        self.txt:SetFont(self.Font)
        self.txt:SetText("")
        self.txt:SetTextColor(Color(255,255,255))
        self.txt:SetAlpha(0)
        self.txt:SetEditable(true)
        self.txt:SetNumeric( self.Numeric )
        self.txt:SetDrawBackground(false)
        self.txt:SetEnterAllowed( true )
        self.txt:SetUpdateOnType( true )
        self.Text = self.ClickText
        self.txt:MakePopup()
        lobby.keepopen = true
      end

      self.txt.OnValueChange = function(s, string)
        self.Text = self.txt:GetValue()
        self.OnType(self, string)
        self.txt:SetCaretPos( string:len() )
      end

      self.txt.OnEnter = function()
        self.editing = false
        self.OnEnter(self)
        self.txt:Remove()
        lobby.keepopen = false
      end
    end
    self.Button:SetAlpha(255)
    self.Button.OnCursorEntered = function(self)
      alpha = self:GetParent().Color.a+50
      alphabut:AlphaTo( alpha, 0.3 , 0)
    end

    self.Button.OnCursorExited = function(self)
      alpha = self:GetParent().Color.a
      alphabut:AlphaTo( alpha, 0.3 , 0)
    end

    self.Button.Paint = function(self, w, h)

      surface.SetDrawColor( Color(colorbut:GetColor().r,colorbut:GetColor().g,colorbut:GetColor().b, alphabut:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( self:GetParent().Font )
      self.txtw, self.txth = surface.GetTextSize(self:GetParent().Text)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self:GetParent().Text )
    end

	end,
  Update = function(self)
    if !self.Button then return end
    self.ButtonColor:SetColor(self.Color)

    self.Button.Font = self.Font
    self.Alpha:SetAlpha(self.Color.a)
    self.Button:SetText("")
    self.Button:SetSize(self:GetWide(),self:GetTall())
    /*
      local alphabut = self.Alpha
    local colorbut = self.ButtonColor
    self.Button.Paint = function(self, w, h)

      surface.SetDrawColor( Color(colorbut:GetColor().r,colorbut:GetColor().g,colorbut:GetColor().b, alphabut:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( self:GetParent().Font )
      self.txtw, self.txth = surface.GetTextSize(self:GetParent().Text)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self:GetParent().Text )
    end
    */
  end,
  SetFont = function(self, font)
    self.Font = font
    self:Update()
  end,

  SetText = function(self, txt)
    self.Text = txt
    self.DefText = txt
    self:Update()
  end,

  SetColor = function(self, col)
    self.Color = col
    self:Update()
  end,

  SetHighlight = function(self, h)
    self.Highlight = h
    self:Update()
  end,



	Paint = function( self, w, h )
	end,

  Think = function( self, w, h )
  end
}

TEXTEDIT = vgui.RegisterTable( TEXTEDIT, "EditablePanel" )

local LOBBY_BUTTON =
{
	Init = function( self )
    self.Host = ""
    self.Mode = ""
    self.Name = ""
    self.Players = ""
    self.Settings = {}

  end,

  Setup = function( self, lb, lst, id )

		self.Host = lb["lobbyHost"]
    self.Mode = lb["lobbyMode"]
    self.Name = lb["lobbyName"]
    self.Players = lb["lobbyPlayers"]
    self.Settings = lb["lobbySettings"]
    self.List = lst
    self.LobbyID = id
		self:Think( self )
    local alpha = 0

    self.ButtonColor = self:Add( "DModelPanel" )

    self.ButtonColor:SetAlpha(255)
    self.ButtonColor:SetColor(lobby.Minigames[self.Mode])
    self.ButtonColor:SetSize( 0, 0 )

    self.Button = self:Add( "DButton" )
    self.Alpha = vgui.Create("DButton")
    self.Alpha:SetSize(0,0)
    self.Button.Name = self.Name
    self.Button.Host = self.Host
    self.Alpha:SetAlpha(10)

    self.Alpha:SetText("")
    self.Alpha.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, 255) )
      surface.DrawRect(0, 0, w, h)
    end
    self.Button:SetText("")
    local alphabut = self.Alpha
    local colorbut = self.ButtonColor
    self.Button:SetSize(self:GetWide(),self:GetTall())
    --self.Button:Dock(TOP)
    self.Button.DoClick = function()
      alphabut:SetAlpha(250)
      alphabut:AlphaTo( alpha, 0.2, 0)
      colorbut:SetColor(Color(255,255,255))

      colorbut:ColorTo(lobby.Minigames[lb["lobbyMode"]], 0.2)
      if !table.HasValue(self.Players, LocalPlayer()) then
        net.Start("lobbyNetworking")
          net.WriteString("join")

          net.WriteTable( {[1] = LocalPlayer(), [2] = self.LobbyID, [3] = false} ) --replace host with lobbyid
        net.SendToServer()
      else
        net.Start("lobbyNetworking")
          net.WriteString("leave")
          net.WriteTable( {[1] = LocalPlayer()} )
        net.SendToServer()
      end
    end
    self.Button:SetAlpha(255)
    self.Button.OnCursorEntered = function(self)
      alphabut:AlphaTo( 120, 0.3 , 0)
      alpha = 120

    end

    self.Button.OnCursorExited = function(self)
      alphabut:AlphaTo( 10, 0.3 , 0)
      alpha = 10
    end

    self.Button.Paint = function(self, w, h)
      surface.SetDrawColor( Color(colorbut:GetColor().r,colorbut:GetColor().g,colorbut:GetColor().b, alphabut:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( "HUD Menu" )
      self.txtw, self.txth = surface.GetTextSize(self.Name)
      surface.SetTextPos( (w/2)- self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self.Name )

    end

	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    if !lobby.lobbies[self.LobbyID] then
      self.Button.Paint = function() end
      self.ButtonColor:Remove()
      self.Button:Remove()
      self:Remove()
    return end

    if lobby.lobbies[self.LobbyID]["lobbyHost"] != self.Host or lobby.lobbies[self.LobbyID]["lobbyMode"] != self.Mode or lobby.lobbies[self.LobbyID]["lobbySettings"] != self.Settings then
      self.Button.Paint = function() end
      self.ButtonColor:Remove()
      self.Button:Remove()
      self:Remove()
			return
		end
  end
}

LOBBY_BUTTON = vgui.RegisterTable( LOBBY_BUTTON, "EditablePanel" )

local MINIGAME_BUTTON =
{
	Init = function( self )
    self.Minigame = "None"
    self.func = function() end
    local alpha = 0
    self.Alpha = self:Add( "DButton" )
    self.Alpha:SetAlpha(0)
    self.Alpha:SetText("")
    self.Alpha.Paint = function(self, w, h)
    end

    self.TextAlpha = self:Add( "DModelPanel" )

    self.TextAlpha:SetAlpha(255)
    self.TextAlpha:SetColor(lobby.Minigames[self.Minigame])
    self.TextAlpha:SetSize( 0, 0 )

    self.Button = self:Add( "DButton" )
    self.Button:SetPos( 0, 0 )
    self.Button:SetSize( (850+200 - 210 - 30)/5, 100 )
    self.Button:SetText("")
    self.Button.DoClick = function()
      if self.Minigame == "None" then return end
      self.Alpha:SetAlpha(250)
      self.Alpha:AlphaTo( alpha, 0.2, 0)
      self.funct = self.func(self)
    end
    self.Button:SetAlpha(255)
    self.Button.OnCursorEntered = function(self)
      self:GetParent().Alpha:AlphaTo( 200, 0.3 , 0)
      self:GetParent().TextAlpha:ColorTo(Color(255,255,255), 0.3)
      alpha = 200
    end

    self.Button.OnCursorExited = function(self)
      self:GetParent().Alpha:AlphaTo( 0, 0.3 , 0)
      self:GetParent().TextAlpha:ColorTo(lobby.Minigames[self:GetParent().Minigame], 0.3)
      alpha = 0
    end

    self.Button.Paint = function(self, w, h)
      surface.SetDrawColor( Color(25,25,25, self:GetParent().Alpha:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(self:GetParent().TextAlpha:GetColor())
      surface.SetFont( "HUD Menu2" )
      self.txtw, self.txth = surface.GetTextSize(self:GetParent().Minigame)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self:GetParent().Minigame )
    end
  end,
  ReColor = function(self)
    self.TextAlpha:SetColor(lobby.Minigames[self.Minigame])
  end,
	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
  end
}

MINIGAME_BUTTON = vgui.RegisterTable( MINIGAME_BUTTON, "EditablePanel" )

/*
███    ███ ███████ ███    ██ ██    ██
████  ████ ██      ████   ██ ██    ██
██ ████ ██ █████   ██ ██  ██ ██    ██
██  ██  ██ ██      ██  ██ ██ ██    ██
██      ██ ███████ ██   ████  ██████
*/


local MENU =
{
	Init = function( self )
    // Main Menu
    self.Main= self:Add( "DFrame" )

    self.Main:SetPos( 0, 50 )
    self.Main:SetSize(400, ScrH()/1.015)
    self.Main:SetTitle("")
    self.Main:SetDraggable(false)
    self.Main:ShowCloseButton( false )
    self.Main.Paint = function(self, w, h )
    end
    local ButtonCol = Color(15,15,15,170)
    local ButtonHighCol = Color(255,255,255)

    /*
    ██       ██████  ██████  ██████  ██ ███████ ███████
    ██      ██    ██ ██   ██ ██   ██ ██ ██      ██
    ██      ██    ██ ██████  ██████  ██ █████   ███████
    ██      ██    ██ ██   ██ ██   ██ ██ ██           ██
    ███████  ██████  ██████  ██████  ██ ███████ ███████
    */
    self.Lobbies= self:Add( "EditablePanel" )
    self.Lobbies:SetPos( 0, 50 )
    self.Lobbies:SetSize(ScrW(), ScrH()-120)
    self.Lobbies:SetAlpha(0)
    self.Lobbies:Hide()
    self.Lobbies.Paint = function(self, w, h )
    end

    self.Lobbies.Minigames= self.Lobbies:Add( "EditablePanel" )
    self.Lobbies.Minigames:SetPos( 0, 0 )
    self.Lobbies.Minigames:SetSize((850+200), 50)
    self.Lobbies.Minigames:SetAlpha(255)
    self.Lobbies.Minigames.Paint = function(self, w, h )
      --surface.SetDrawColor( Color(15,15,15,170) )
      --surface.DrawRect(0, 0, w, h)
    end
    for minigame, col in pairs(lobby.Minigames) do

      if minigame != "None" then

        self.Lobbies.Minigames[minigame] = vgui.CreateFromTable( BUTTON, self.Lobbies.Minigames )
        self.Lobbies.Minigames[minigame]:SetPos( 0, 0 )
        self.Lobbies.Minigames[minigame]:SetSize((850+200)/(table.Count(lobby.Minigames)-1), 50)
        self.Lobbies.Minigames[minigame]:SetText(minigame)
        self.Lobbies.Minigames[minigame]:Dock(RIGHT)
        self.Lobbies.Minigames[minigame]:SetColor(ButtonCol)
        self.Lobbies.Minigames[minigame]:SetHighlight(col)
        self.Lobbies.Minigames[minigame].Button.OnClick = function()
        local par = self:GetParent():GetParent()
        local Settings = nil
        net.Start("lobbyNetworking")
          net.WriteString("create")
          net.WriteTable({LocalPlayer(), minigame, Settings})
        net.SendToServer()
        end
      end
    end



    self.Lobbies.Frame= self.Lobbies:Add( "EditablePanel" )
    self.Lobbies.Frame:SetPos( 0, 50 )
    self.Lobbies.Frame:SetSize(ScrW(), ScrH()-(120-50))
    self.Lobbies.Frame:SetAlpha(255)
    self.Lobbies.Frame.Paint = function(self, w, h )
      --surface.SetDrawColor( Color(15,15,15,170) )
      --surface.DrawRect(0, 0, w, h)
    end
    /*
    self.Lobbies.Info = self.Lobbies.Frame:Add( "DFrame" )
    self.Lobbies.Info:SetPos( 0, 0 )
    self.Lobbies.Info:SetSize(ScrW(), 1)
    self.Lobbies.Info:SetTitle("")
    self.Lobbies.Info:SetDraggable(false)
    self.Lobbies.Info:SetAlpha(255)
    self.Lobbies.Info:ShowCloseButton( false )
    self.Lobbies.Info.Paint = function(self, w, h )
      surface.SetDrawColor( Color(15,15,15,170) )
      surface.DrawRect(0, 0, w, h)
    end*/
    self.Lobbies.List = vgui.Create("DScrollPanel", self.Lobbies.Frame)
    --self.Lobbies.List:SetSize(200, ScrH()/1.015-50)
    --self.Lobbies.List:SetPos(20, 5+50)
    self.Lobbies.List:Dock( FILL )
    self.Lobbies.List:SetVerticalScrollbarEnabled(false)

    self.Lobbies.List.VBar:SetAlpha(0)

    self.Lobbies.List.offset = 0

    self.Lobbies.List.Paint = function(self, w, h )

    end

    self.Lobbies.List.OnVScroll = function(self, iOffset )
      self.offset = iOffset
    end

    self.Lobbies.List.Think = function(self )
      self.offsetsmooth = self.offsetsmooth and UT:Lerp(0.03, self.offsetsmooth , self.offset) or self.offset

      self.pnlCanvas:SetPos( 0, self.offsetsmooth )
    end

    self.Lobbies.List.PerformLayout = function(self )
      local wide = self:GetWide()

      self:Rebuild()

      self.VBar:SetUp( self:GetTall(), self.pnlCanvas:GetTall() )
      self.VBar:SetSize(0,0)
      if self.VBar.Enabled then wide = wide end-- self.VBar:GetWide() end

      self.pnlCanvas:SetWide(wide)

      self:Rebuild()
    end

    self.Lobbies.Frame.Topline = self.Lobbies.Frame:Add( "DFrame" )
    self.Lobbies.Frame.Topline:SetPos( 0, 0 )
    self.Lobbies.Frame.Topline:SetSize(ScrW(), 1)
    self.Lobbies.Frame.Topline:SetTitle("")
    self.Lobbies.Frame.Topline:SetDraggable(false)
    self.Lobbies.Frame.Topline:SetAlpha(255)
    self.Lobbies.Frame.Topline:ShowCloseButton( false )
    self.Lobbies.Frame.Topline.Paint = function(self, w, h )
      surface.SetDrawColor( Color(255,255,255,255) )
      surface.DrawRect(0, 0, w, h)
    end

    self.LobbiesBut = vgui.CreateFromTable( BUTTON, self.Main )
    self.LobbiesBut:SetPos( 0, 0 )
    self.LobbiesBut:SetSize(300, 40)
    self.LobbiesBut:SetText("Lobbies")
    self.LobbiesBut:SetColor(ButtonCol)
    self.LobbiesBut:SetHighlight(ButtonHighCol)
    self.LobbiesBut.Button.OnClick = function()
      self.Lobbies:Show()
      self.Main:AlphaTo(0, 0.15, 0)
      self.Lobbies:AlphaTo(255, 0.15, 0)
    end

    /*
    ███████ ███████ ████████ ████████ ██ ███    ██  ██████  ███████
    ██      ██         ██       ██    ██ ████   ██ ██       ██
    ███████ █████      ██       ██    ██ ██ ██  ██ ██   ███ ███████
         ██ ██         ██       ██    ██ ██  ██ ██ ██    ██      ██
    ███████ ███████    ██       ██    ██ ██   ████  ██████  ███████
    */


    self.Settings= self:Add( "DFrame" )

    self.Settings:SetPos( 0, 50 )
    self.Settings:SetSize(400, ScrH()/1.015)
    self.Settings:SetTitle("")
    self.Settings:SetDraggable(false)
    self.Settings:SetAlpha(0)
    self.Settings:Hide()
    self.Settings:ShowCloseButton( false )
    self.Settings.Paint = function(self, w, h )
    end

    // HUD
    self.Settings.HUD = self:Add( "DFrame" )

    self.Settings.HUD:SetPos( 0, 50 )
    self.Settings.HUD:SetSize(400, ScrH()/1.015)
    self.Settings.HUD:SetTitle("")
    self.Settings.HUD:SetDraggable(false)
    self.Settings.HUD:SetAlpha(0)
    self.Settings.HUD:Hide()
    self.Settings.HUD:ShowCloseButton( false )
    self.Settings.HUD.Paint = function(self, w, h )
    end

    self.Settings.HUD.Toggle = vgui.CreateFromTable( BUTTON, self.Settings.HUD )
    self.Settings.HUD.Toggle:SetPos( 0, (40+5)*0 )
    self.Settings.HUD.Toggle:SetSize(300, 40)
    self.Settings.HUD.Toggle:SetText("HUD")
    self.Settings.HUD.Toggle:SetColor(ButtonCol)
    self.Settings.HUD.Toggle:SetHighlight(ButtonHighCol)
    self.Settings.HUD.Toggle.Button.OnClick = function()
      local convar = GetConVar("grp_hud")
      local float = convar:GetInt()+1
      if float > 1 then
        float = 0
      end
      convar:SetInt( float )
    end

    self.Settings.HUD.KeyEchoes = vgui.CreateFromTable( BUTTON, self.Settings.HUD )
    self.Settings.HUD.KeyEchoes:SetPos( 0, (40+5)*1 )
    self.Settings.HUD.KeyEchoes:SetSize(300, 40)
    if GetConVar("grp_keys"):GetInt() == 0 then
      self.Settings.HUD.KeyEchoes:SetText("KeyEchoes: None")
    elseif GetConVar("grp_keys"):GetInt() == 1 then
      self.Settings.HUD.KeyEchoes:SetText("KeyEchoes: Standard")
    elseif GetConVar("grp_keys"):GetInt() == 2 then
      self.Settings.HUD.KeyEchoes:SetText("KeyEchoes: Minimalist")
    end
    self.Settings.HUD.KeyEchoes:SetColor(ButtonCol)
    self.Settings.HUD.KeyEchoes:SetHighlight(ButtonHighCol)
    self.Settings.HUD.KeyEchoes.Button.OnClick = function()
      local convar = GetConVar("grp_keys")
      local float = convar:GetInt()+1
      if float > 2 then
        float = 0
      end
      convar:SetInt( float )
      if float == 0 then
        self.Settings.HUD.KeyEchoes:SetText("KeyEchoes: None")
      elseif float == 1 then
        self.Settings.HUD.KeyEchoes:SetText("KeyEchoes: Standard")
      elseif float == 2 then
        self.Settings.HUD.KeyEchoes:SetText("KeyEchoes: Minimalist")
      end
    end

    self.Settings.HUD.SpeedoMeter = vgui.CreateFromTable( BUTTON, self.Settings.HUD )
    self.Settings.HUD.SpeedoMeter:SetPos( 0, (40+5)*2 )
    self.Settings.HUD.SpeedoMeter:SetSize(300, 40)
    if GetConVar("grp_speedometer"):GetInt() == 0 then
      self.Settings.HUD.SpeedoMeter:SetText("SpeedoMeter: Standard")
    elseif GetConVar("grp_speedometer"):GetInt() == 1 then
      self.Settings.HUD.SpeedoMeter:SetText("SpeedoMeter: Graph")
    end
    self.Settings.HUD.SpeedoMeter:SetColor(ButtonCol)
    self.Settings.HUD.SpeedoMeter:SetHighlight(ButtonHighCol)
    self.Settings.HUD.SpeedoMeter.Button.OnClick = function()
      local convar = GetConVar("grp_speedometer")
      local float = convar:GetInt()+1
      if float > 1 then
        float = 0
      end
      convar:SetInt( float )
      if float == 0 then
        self.Settings.HUD.SpeedoMeter:SetText("SpeedoMeter: Standard")
      elseif float == 1 then
        self.Settings.HUD.SpeedoMeter:SetText("SpeedoMeter: Graph")
      end
    end

    self.Settings.HUD.Lag = vgui.CreateFromTable( TEXTEDIT, self.Settings.HUD )
    self.Settings.HUD.Lag:SetPos( 0, (40+5)*3 )
    self.Settings.HUD.Lag:SetSize(300, 40)
    self.Settings.HUD.Lag:SetText("Hud Sensitivity: " .. GetConVar("grp_hud_lag_sensitivity"):GetInt())
    self.Settings.HUD.Lag:SetColor(ButtonCol)
    self.Settings.HUD.Lag:SetHighlight(ButtonHighCol)
    self.Settings.HUD.Lag.Numeric = true
    self.Settings.HUD.Lag.ClickText = "Hud Sensitivity: "
    self.Settings.HUD.Lag.Min = 1
    self.Settings.HUD.Lag.Max = 30
    self.Settings.HUD.Lag.Button.OnClick = function()
    end

    self.Settings.HUD.Lag.OnEnter = function(self)
      if tonumber(self.txt:GetValue()) == nil or tonumber(self.txt:GetValue()) < self.Min then
        local string = tostring(self.Min)
        self.Text = "Hud Sensitivity: " .. string
        self.txt:SetText(string)
      end
      GetConVar("grp_hud_lag_sensitivity"):SetInt(self.txt:GetText())
    end

    self.Settings.HUD.Lag.OnType = function(self, string)

      self.Text = "Hud Sensitivity: " .. string

      if tonumber(string) and tonumber(string) > self.Max then
        string = tostring(self.Max)
        self.Text = "Hud Sensitivity: " .. string
        self.txt:SetText(string)
      end
      --self.txt:SetText(string)
    end

    self.Settings.HUD.Crosshair = vgui.CreateFromTable( BUTTON, self.Settings.HUD )
    self.Settings.HUD.Crosshair:SetPos( 0, (40+5)*4 )
    self.Settings.HUD.Crosshair:SetSize(300, 40)
    self.Settings.HUD.Crosshair:SetText("Crosshair")
    self.Settings.HUD.Crosshair:SetColor(ButtonCol)
    self.Settings.HUD.Crosshair:SetHighlight(ButtonHighCol)
    self.Settings.HUD.Crosshair.Button.OnClick = function()
      local convar = GetConVar("grp_crosshair")
      local float = convar:GetInt()+1
      if float > 2 then
        float = 0
      end
      convar:SetInt( float )
    end

    self.Settings.HUD.Back = vgui.CreateFromTable( BUTTON, self.Settings.HUD )
    self.Settings.HUD.Back:SetPos( 0, (40+5)*7 )
    self.Settings.HUD.Back:SetSize(300, 40)
    self.Settings.HUD.Back:SetText("Back")
    self.Settings.HUD.Back:SetColor(ButtonCol)
    self.Settings.HUD.Back:SetHighlight(ButtonHighCol)
    self.Settings.HUD.Back.Button.OnClick = function()
      self.Settings:AlphaTo(255, 0.15, 0)
      self.Settings.HUD:AlphaTo(0, 0.15, 0)
      timer.Simple(0.16, function() self.Settings.HUD:Hide() end)
    end

    self.Settings.HUDBut = vgui.CreateFromTable( BUTTON, self.Settings )
    self.Settings.HUDBut:SetPos( 0, 0 )
    self.Settings.HUDBut:SetSize(300, 40)
    self.Settings.HUDBut:SetText("HUD")
    self.Settings.HUDBut:SetColor(ButtonCol)
    self.Settings.HUDBut:SetHighlight(ButtonHighCol)
    self.Settings.HUDBut.Button.OnClick = function()
      self.Settings.HUD:Show()
      self.Settings.HUD:AlphaTo(255, 0.15, 0)
      self.Settings:AlphaTo(0, 0.15, 0)

    end

    self.Settings.Back = vgui.CreateFromTable( BUTTON, self.Settings )
    self.Settings.Back:SetPos( 0, (40+5) )
    self.Settings.Back:SetSize(300, 40)
    self.Settings.Back:SetText("Back")
    self.Settings.Back:SetColor(ButtonCol)
    self.Settings.Back:SetHighlight(ButtonHighCol)
    self.Settings.Back.Button.OnClick = function()
      self.Main:AlphaTo(255, 0.15, 0)
      self.Settings:AlphaTo(0, 0.15, 0)
      timer.Simple(0.16, function() self.Settings:Hide() end)
    end

    self.SettingsBut = vgui.CreateFromTable( BUTTON, self.Main )
    self.SettingsBut:SetPos( 0, 40+5 )
    self.SettingsBut:SetSize(300, 40)
    self.SettingsBut:SetText("Settings")
    self.SettingsBut:SetColor(ButtonCol)
    self.SettingsBut:SetHighlight(ButtonHighCol)
    self.SettingsBut.Button.OnClick = function()
      self.Settings:Show()
      self.Main:AlphaTo(0, 0.15, 0)
      self.Settings:AlphaTo(255, 0.15, 0)
    end

    /*
    ██      ███████  █████  ██████  ███████ ██████  ██████   ██████   █████  ██████  ██████
    ██      ██      ██   ██ ██   ██ ██      ██   ██ ██   ██ ██    ██ ██   ██ ██   ██ ██   ██
    ██      █████   ███████ ██   ██ █████   ██████  ██████  ██    ██ ███████ ██████  ██   ██
    ██      ██      ██   ██ ██   ██ ██      ██   ██ ██   ██ ██    ██ ██   ██ ██   ██ ██   ██
    ███████ ███████ ██   ██ ██████  ███████ ██   ██ ██████   ██████  ██   ██ ██   ██ ██████
    */


    self.LeaderBut = vgui.CreateFromTable( BUTTON, self.Main )
    self.LeaderBut:SetPos( 0, (40+5)*2 )
    self.LeaderBut:SetSize(300, 40)
    self.LeaderBut:SetText("Leaderboards")
    self.LeaderBut:SetColor(ButtonCol)
    self.LeaderBut:SetHighlight(ButtonHighCol)
    self.LeaderBut.Button.OnClick = function()
      self.Main:AlphaTo(0, 0.3, 0)
    end

    /*
    ███████ ████████  █████  ████████ ███████
    ██         ██    ██   ██    ██    ██
    ███████    ██    ███████    ██    ███████
         ██    ██    ██   ██    ██         ██
    ███████    ██    ██   ██    ██    ███████
    */


    self.Stats = vgui.CreateFromTable( BUTTON, self.Main )
    self.Stats:SetPos( 0, (40+5)*3 )
    self.Stats:SetSize(300, 40)
    self.Stats:SetText("Stats")
    self.Stats:SetColor(ButtonCol)
    self.Stats:SetHighlight(ButtonHighCol)
    self.Stats.Button.OnClick = function()
      self.Main:AlphaTo(0, 0.3, 0)
    end

    /*
    ██   ██ ███████ ██      ██████
    ██   ██ ██      ██      ██   ██
    ███████ █████   ██      ██████
    ██   ██ ██      ██      ██
    ██   ██ ███████ ███████ ██
    */



    self.Help= self:Add( "DFrame" )
    self.Help:SetPos( 0, 50 )
    self.Help:SetSize(400, ScrH()/1.015)
    self.Help:SetTitle("")
    self.Help:SetDraggable(false)
    self.Help:SetAlpha(0)
    self.Help:Hide()
    self.Help:ShowCloseButton( false )
    self.Help.Paint = function(self, w, h )
    end

    self.Help.Back = vgui.CreateFromTable( BUTTON, self.Help )
    self.Help.Back:SetPos( 0, (40+5) )
    self.Help.Back:SetSize(300, 40)
    self.Help.Back:SetText("Back")
    self.Help.Back:SetColor(ButtonCol)
    self.Help.Back:SetHighlight(ButtonHighCol)
    self.Help.Back.Button.OnClick = function()
      self.Main:AlphaTo(255, 0.15, 0)
      self.Help:AlphaTo(0, 0.15, 0)
      timer.Simple(0.16, function() self.Help:Hide() end)
    end


    self.HelpBut = vgui.CreateFromTable( BUTTON, self.Main )
    self.HelpBut:SetPos( 0, (40+5)*4 )
    self.HelpBut:SetSize(300, 40)
    self.HelpBut:SetText("Help")
    self.HelpBut:SetColor(ButtonCol)
    self.HelpBut:SetHighlight(ButtonHighCol)
    self.HelpBut.Button.OnClick = function()
      self.Help:Show()
      self.Main:AlphaTo(0, 0.15, 0)
      self.Help:AlphaTo(255, 0.15, 0)
    end

    /*
    self.Admin = vgui.CreateFromTable( BUTTON, self.Main )
    self.Admin:SetPos( 0, (40+5)*5 )
    self.Admin:SetSize(300, 40)
    self.Admin:SetText("Admin")
    self.Admin:SetColor(ButtonCol)
    self.Admin:SetHighlight(ButtonHighCol)
    */

  end,

	PerformLayout = function( self )

		self:SetSize( 850+200, ScrH() )
		self:SetPos( ScrW()/2 - ((850+200)/2), 0 )
	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    for id, lb in pairs( lobby.lobbies ) do

			if ( IsValid( lb.Entry ) ) then continue end
      if ( IsValid( lb.Entry2 ) ) then continue end

			lb.Entry = vgui.CreateFromTable( LOBBY_BUTTON, lb.Entry )
      lb.Entry:SetSize((850+200), 30)
      lb.Entry:Dock(TOP)
      lb.Entry:DockMargin(0,0,0,5)
			lb.Entry:Setup( lb, self.Lobbies.List, id )

      self.Lobbies.List:AddItem( lb.Entry  )


		end
	end
}

MENU = vgui.RegisterTable( MENU, "EditablePanel" )

if ( IsValid( LobbyMenu ) ) then
	LobbyMenu:Remove()
end
function GM:OnSpawnMenuOpen()
	open = true

	if ( !IsValid( LobbyMenu ) ) then
		LobbyMenu = vgui.CreateFromTable( MENU )
    LobbyMenu:SetAlpha(0)
	end

	if ( IsValid( LobbyMenu ) ) then
    LobbyMenu:AlphaTo(255, 0.15)
		LobbyMenu:Show()
		LobbyMenu:MakePopup()
		LobbyMenu:SetKeyboardInputEnabled( false )
	end
end

function GM:OnSpawnMenuClose()
  if lobby.keepopen then return end
	open = false

	if ( IsValid( LobbyMenu ) ) then
		timer.Simple(0.2, function()
			if not open then
				LobbyMenu:Hide()
			end
		end)

		LobbyMenu:AlphaTo(0, 0.15)
	end

end
/*
concommand.Add("hudeditor", function(ply, command, args)

  hEdit.Editor(true)
end)*/
