local Move = FindMetaTable( "CMoveData" )

function PhysicsClipVelocity( inv, normal, out, overbounce )
	local	backoff
	local	change = 0
	local	angle
	local	i

	local STOP_EPSILON = 0.1

	angle = normal.z

	backoff = inv:Dot( normal ) * overbounce
	for i = 1 , 3 do
		change = normal[i] * backoff
    out[i] =inv[i] -change
		if out[i] > -STOP_EPSILON and out[i] < STOP_EPSILON then
			out[i] = 0
		end
	end
end

function canSlide(ply, tr, vel)

	-- This checks which direction on the ramp the player is moving
	if Vector(tr.HitNormal.x, tr.HitNormal.y):Dot(Vector(vel.x, vel.y):GetNormalized()) < 0
	and ( tr.HitNormal.z < 1 )
	and ( tr.HitNormal.z > ply:SlideNormal() )
	then
		return true
	end
	return false
end

function Move:SRSlideTest( trace, vecVelocity, ply )
  local origVel 			 = vecVelocity -- Original velocity.
	local origin				 = self:GetOrigin()
	local minSlideVel		 = 140
	local vecAbsVelocity = Vector()
	local change

	//Transfer Vertical to Horizontal momentum
	--local newVel = vecVelocity*Vector(0, 0, 2)
  // A backoff of 1.0 is a slide.
  // Anything above 1 makes players bounce.
  local flBackOff = 1

	if trace.HitNormal.z <= ply:SlideNormal() // Dont apply the physics on surf ramps
	or trace.HitNormal.z == 1	// Or when on flat ground
	or !self:KeyDown(IN_DUCK) // Or when not holding duck
	or Vector(trace.HitNormal.x, trace.HitNormal.y):Dot(Vector(vecVelocity.x, vecVelocity.y):GetNormalized()) < 0  then // Or when going up a slope
		ply.TestSlide = false
		return
	end

	if !ply.TestSlide then
		ply.TestSlide = false

	else
		PhysicsClipVelocity( vecVelocity, trace.HitNormal, vecAbsVelocity, flBackOff )
	end

	//Gotta go down at 1000u/s to slide down them
	if (( trace.HitNormal.z < 1 ) and vecVelocity.z < -700) or ply.TestSlide then

		if !ply.TestSlide then
			local moveDir = vecVelocity*Vector(1,1,0)
			moveDir = moveDir	:GetNormalized()
			// Changes from vertical to horizontal velocity
			local newVel = Vector((moveDir.x*vecVelocity:Length()), (moveDir.y*vecVelocity:Length()), 0)
			PhysicsClipVelocity( newVel, trace.HitNormal, vecAbsVelocity, flBackOff )
		end
		local pEntity  = trace.Entity
		origin.z = trace.HitPos.z+2
		ply:SetGroundEntity( NULL )

		vecVelocity    = vecAbsVelocity + ply:GetBaseVelocity()
		ply:Sliding(true)
		if vecVelocity.z*FrameTime()-origVel.z*FrameTime() < 0 then
			self:SetVelocity( origVel )
			self:SetOrigin(origin)
		else
			self:SetVelocity( vecVelocity )
			self:SetOrigin(origin)
		end
		ply.TestSlide = true
		if self:KeyPressed(IN_JUMP) then
			self:SetVelocity((self:GetVelocity()*Vector(1,1,0)) + Vector(0,0,ply:GetJumpPower()))
		end
	end
end

function Move:resolveFlyCollisionSlide( trace, vecVelocity, ply )
  local origVel 			 = vecVelocity -- Original velocity.
	local origin				 = self:GetOrigin()
	local minSlideVel		 = 120
  local slideVelSqr 	 = (30*30)  -- The speed required for sliding on ramps.
	local vecAbsVelocity = Vector()
  // A backoff of 1.0 is a slide.
  // Anything above 1 makes players bounce.
  local flBackOff = 1

	// Changes the velocity "direction"

  PhysicsClipVelocity( vecVelocity, trace.HitNormal, vecAbsVelocity, flBackOff )

	// Slide up ramps that you can walk on when going minSlideVel

	if canSlide(ply, trace, vecVelocity) and vecAbsVelocity.z > minSlideVel then
	  // Get the total velocity (player + conveyors, etc.)
	  vecVelocity    = vecAbsVelocity + ply:GetBaseVelocity()
		vecVelocity = vecVelocity
		local flSpeedSqr = vecVelocity:Dot(vecVelocity)
	  local pEntity  = trace.Entity

	  if ( flSpeedSqr < slideVelSqr )  then
	   if trace.Entity then
	     ply:SetGroundEntity( pEntity )
	   end
		 self:SetVelocity( origVel ) // Reset velocity
		 ply:Sliding(false)
	  else
			ply:SetGroundEntity( NULL )
			origin.z = trace.HitPos.z+2
			self:SetVelocity( vecVelocity )
			self:SetOrigin(origin)
			ply:Sliding(true)
	  end
		// Surf ramps
	elseif ( trace.HitNormal.z <= ply:SlideNormal() ) then

		local pEntity  = trace.Entity
		origin.z = trace.HitPos.z+2
		ply:SetGroundEntity( NULL )
		vecVelocity    = vecAbsVelocity + ply:GetBaseVelocity()
		ply:Sliding(true)
		if vecVelocity.z*FrameTime()-origVel.z*FrameTime() < 0 then
			self:SetVelocity( origVel )
			self:SetOrigin(origin)
		else
			self:SetVelocity( vecVelocity )
			self:SetOrigin(origin)
		end

	end
end

function rampSlide(ply, mv)
	local Pos = mv:GetOrigin()
  local Mins = ply:OBBMins()
  local Maxs = ply:OBBMaxs()
  local endPos = Pos *1
  local vel = mv:GetVelocity()
	local moveDir = mv:GetVelocity():GetNormalized()
	// Predicted next frame movement if going quickly downwards
	endPos.z = (endPos.z -2) - (math.Clamp(vel.z*FrameTime(), -vel.z*FrameTime(), 0)) // trace a bit further than feet
  local tr = {
    start = Pos,
    endpos = endPos,
    --mins = Mins,
    --maxs = Maxs,
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }

local tL = util.TraceEntity( tr , ply)

	if !ply:Sliding() then
		endPos.x = (endPos.x) +vel.x*FrameTime()
		endPos.y = (endPos.y) + vel.y*FrameTime()
	end

  local trBkup = {
    start = Pos,
		endpos = endPos,
    --mins = Mins,
    --maxs = Maxs,
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }

	local tLBkup = util.TraceEntity( tr, ply )

	if (tL.HitWorld and tL.HitNormal.z < 1 and tL.HitNormal.z > 0)  then
			mv:resolveFlyCollisionSlide( tL, vel, ply )
			mv:SRSlideTest( tL, vel, ply )

	elseif (tLBkup.HitWorld and tLBkup.HitNormal.z < 1 and tLBkup.HitNormal.z > 0) then
			mv:resolveFlyCollisionSlide( tLBkup, vel, ply )
			mv:SRSlideTest( tLBkup, vel, ply )
	else
		ply.TestSlide = false
		ply:Sliding(false)
	end
end
hook.Add("PlayerTick", "Rampslide Fix", rampSlide)
