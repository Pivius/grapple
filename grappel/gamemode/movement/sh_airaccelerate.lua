if SERVER then
  util.AddNetworkString( "syncAirAccel" )
end
if CLIENT then
  net.Receive("syncAirAccel", function()
    local ply = net.ReadEntity()
    local float = net.ReadFloat()
    local float2 = net.ReadFloat()
    ply:AirAccel(float)
    if float2 then
      ply:Gain(float2)
    end
  end)
end

-- GLua version of airaccelerate in src engine.
function AirAccelerate( ply, mv, cmd, accel, gain )

  if !ply:Alive() or ply:OnGround() or ply:WaterLevel() > 0 then return end

  

	local addSpeed, accelSpeed, wishDir, wishVel, wishSpd, wishSpeed
  local curSpeed = mv:GetVelocity()
  local fmove, smove = mv:GetForwardSpeed(), mv:GetSideSpeed()
  local forward, right = mv:GetMoveAngles():Forward(), mv:GetMoveAngles():Right()
  forward.z, right.z = 0
  forward:Normalize()
  right:Normalize()
  wishVel = (forward*fmove) + (right*smove)
  wishVel.z = 0
  wishSpeed = wishVel:Length()

  if (wishSpeed > mv:GetMaxSpeed()) then
    wishVel = wishVel * (mv:GetMaxSpeed()/wishSpeed)
		wishSpeed = mv:GetMaxSpeed()
  end
  wishSpd = wishSpeed
  wishVel:Normalize()
    if (wishSpd > gain) then
      wishSpd = gain
    end
  wishDir = wishVel
	// Determine veer amount
	curSpeed = (mv:GetVelocity()):Dot(wishDir)

	// See how much to add
  addSpeed = wishSpd-curSpeed

	// If not adding any, done.
	if (addSpeed <= 0) then
		return
  end
  local surfaceFriction = 1
	--// Determine acceleration speed after acceleration
	accelSpeed = accel * wishSpeed * FrameTime() * surfaceFriction

	// Cap it
	if (accelSpeed > addSpeed) then
		accelSpeed = addSpeed
  end

  mv:SetVelocity(mv:GetVelocity() + (accelSpeed * wishDir))
end

hook.Add("FinishMove", "Airaccelerate", function(ply, mv, cmd)
  --ply:SetSaveValue( "m_MoveType", 2 )
  AirAccelerate( ply, mv, cmd, ply:AirAccel(), ply:Gain() )


end)
