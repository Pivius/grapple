if SERVER then
  util.AddNetworkString( "syncAutoHop" )
end
if CLIENT then
  net.Receive("syncAutoHop", function()
    local ply = net.ReadEntity()
    local bool = net.ReadBool()

    ply:AutoHop(bool)

  end)
end

hook.Add( 'SetupMove', 'AutoHop', function( ply, move )
  
  if !ply:IsOnGround() and ply:AutoHop() then

    move:SetButtons( bit.band( move:GetButtons(), bit.bnot( IN_JUMP ) ) )
  end
end )
/*
hook.Add( 'SetupMove', 'EzScroll', function( ply, mv )

  if !ply.groundTick then
    ply.storedVel = Vector(0,0,0)
    ply.groundTick = 0
  end

  if ply:IsOnGround() and ply.groundTick <= ply:MaxBhopTick()+1 then
    ply.groundTick = ply.groundTick+1
    if ply.groundTick == 1 then
      ply.storedVel = mv:GetVelocity()
    end

  elseif !ply:IsOnGround() then

    ply.groundTick = 0
  end
  if (ply.groundTick <= ply:MaxBhopTick() and ply.groundTick >= 1) and mv:KeyPressed(IN_JUMP) then
    mv:SetVelocity(Vector(ply.storedVel.x,ply.storedVel.y, 0))
  end
end )*/
