local misc = {}

misc.desc = "Just a lobby"
misc.teams = {
  [1] = "Miscellaneous"
}

misc.colors = {
  [1] = Color(100,255,255,255),

}

misc.players = {}
misc.settings = {}
misc.timeLimit = 0
misc.gravity = 1
misc.airAccelerate = 10

function misc.ReInit()
  misc.timeLimit = misc.settings["time"]
  misc.gravity = misc.settings["gravity"]/400
  misc.airAccelerate = misc.settings["airaccelerate"]
end

function misc.addPlayer(ply)
  lobby:SetColor(ply, misc.colors[1])
  --ply:TrailColor(misc.colors[1])
  ply:AutoHop(misc.settings["autohop"])
  ply:AirAccel(misc.settings["airaccelerate"])
  net.Start("syncAutoHop")
    net.WriteEntity(ply)
    net.WriteBool(misc.settings["autohop"])
  net.Send(ply)
  net.Start("syncAirAccel")
    net.WriteEntity(ply)
    net.WriteFloat(misc.settings["airaccelerate"])
  net.Send(ply)
  ply:SetGravity( misc.gravity )
  misc.players[ply] = 1 --table.insert(misc.players, ply)
end

function misc.removePlayer(ply)
  --ply:TrailColor(Color(255,255,255))
  lobby:SetColor(ply, Color(255,255,255))
  ply:AutoHop(false)
  net.Start("syncAutoHop")
    net.WriteEntity(ply)
    net.WriteBool(false)
  net.Send(ply)
  ply:AirAccel(10)
  net.Start("syncAirAccel")
    net.WriteEntity(ply)
    net.WriteFloat(10)
  net.Send(ply)
  ply:SetGravity( 1 )
  UT:RemoveByKey(misc.players, ply) --table.RemoveByValue(misc.players, ply)
end

function misc.Think()

end

return misc
