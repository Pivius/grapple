
-------------------------------------
-- Idea SECTION
-------------------------------------
-- SPEED UI
-------------------------------------
-- IDEA The limit should be all time top -- Idea BY THEO

-------------------------------------
-- LOBBIES
-------------------------------------
-- IDEA Hard mode, worse acceleration (higher grav?), no shift

-------------------------------------
-- HEALTH UI
-------------------------------------
--IDEA Light/White UI style
--IDEA Fill up health heart and speed guy idea by metaloexdxdxdxd
-------------------------------------
-- UI.
-------------------------------------
hud = hud or {}
hud.module = include( 'modules/cl_hudmodule.lua' )

Topspeed     = 0

topstartlimit= 5
toplimit     = topstartlimit
-------------------------------------
-- Commands.
-- Different console commands for the UI.
-------------------------------------
local keyechoes   = CreateClientConVar("grp_keys", "0", true, true, "Hides/shows key echoes. 1 = fancy echoes, 2 = quake style echoes")
local outlineecho   = CreateClientConVar("grp_keys_outline", "0", true, true, "Enables outlined key echoes")
local hudtoggle   = CreateClientConVar("grp_hud", "1", true, true, "Hides/shows hud")
local indicators   = CreateClientConVar("grp_indicators", "1", true, true, "Hides/shows player indicators")
local crosshairtoggle = CreateClientConVar( "grp_crosshair", "1", true, true, "Hides/shows crosshair")
local sGraph = CreateClientConVar( "grp_speedometer", "1", true, true, "Hides/shows graph")
local LagSens = CreateClientConVar( "grp_hud_lag_sensitivity", "10", true, true, "Hud lag sensitivity. Default: 10")
local velo = CreateClientConVar( "grp_2Dvel", "0", true, true, "0 = Normal, 1 = 2D Velocity")

cvars.AddChangeCallback( "grp_hud_lag_sensitivity", function( convar_name, value_old, value_new )
  if tonumber(value_new) > 30 then
    LagSens:SetInt(30)
    return
  elseif tonumber(value_new) < 1 then
    LagSens:SetInt(1)
    return
  end
  local ls = math.Clamp(LagSens:GetInt(),1,30)

	speedDisplay.tiltRate = math.Clamp(ls,1,30)/4
  speedDisplay.resetRate = 40/((ls/4)/2)
  keyEchoes.tiltRate = ls/4
  keyEchoes.resetRate = 40/((ls/4)/2)
end )
-------------------------------------
-- The main UI colors.
-------------------------------------
hud.color = Color(255, 255, 255, 220)
hud.customColor = Color(255,255,255)
hud.customColor2 = Color(200,200,200)

hud.keys = {
  ["W"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_FORWARD},
  ["A"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_MOVELEFT},
  ["S"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_BACK},
  ["D"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_MOVERIGHT},
  ["JUMP"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_JUMP},
  ["DUCK"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_DUCK},
  ["M1"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_ATTACK},
  ["M2"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_ATTACK2}
}

-------------------------------------
-- INIT UI.
-------------------------------------
function hud.Init()
  local ls = math.Clamp(LagSens:GetFloat(),1,30)
  --Speedometer
  if speedDisplay then
    speedDisplay:Remove()
  end
  /*
  speedDisplay = vgui.Create( "3DPanel" )

  speedDisplay.Distance = 1.74
  speedDisplay.tiltRate = ls/4
  speedDisplay.resetRate = 40/((ls/4)/2)
  speedDisplay.sideAng = 30
  speedDisplay.moveUpdate = false
  */
  if healthDisplay then
    healthDisplay:Remove()
  end
  /*
  healthDisplay = vgui.Create( "3DPanel" )

  healthDisplay.Distance = 1.74
  healthDisplay.tiltRate = ls/4
  healthDisplay.resetRate = 40/((ls/4)/2)
  healthDisplay.sideAng = 30
  healthDisplay.moveUpdate = false
  */
  --KeyEches
  if keyEchoes then
    keyEchoes:Remove()
  end


  keyEchoes = vgui.Create( "3DPanel" )

  keyEchoes.Distance = 1.65
  keyEchoes.tiltRate = ls/4
  keyEchoes.resetRate = 40/((ls/4)/2)
  keyEchoes.sideAng = 0
  keyEchoes.moveUpdate = false

  if leftSide then
    leftSide:Remove()
  end
  leftSide = vgui.Create( "3DPanel" )

  leftSide.Distance = 1.65
  leftSide.tiltRate = ls/4
  leftSide.resetRate = 40/((ls/4)/2)
  leftSide.sideAng = 0
  leftSide.moveUpdate = false

  if rightSide then
    rightSide:Remove()
  end
  rightSide = vgui.Create( "3DPanel" )

  rightSide.Distance = 1.65
  rightSide.tiltRate = ls/4
  rightSide.resetRate = 40/((ls/4)/2)
  rightSide.sideAng = 0
  rightSide.moveUpdate = false

  if top then
    top:Remove()
  end
  top = vgui.Create( "3DPanel" )

  top.Distance = 1.65
  top.tiltRate = ls/4
  top.resetRate = 40/((ls/4)/2)
  top.sideAng = 0
  top.moveUpdate = false
end
hud.Init()
-------------------------------------
-- Removes standard UI.
-------------------------------------
function GM:HUDDrawTargetID()
end
function GM:HUDShouldDraw(name)
    local draw = true
    if(name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudSecondaryAmmo" or name == "CHudCrosshair") then
    draw = false;
    end
return draw;
end


-------------------------------------
-- Draw Player behind walls.
-------------------------------------
function drawPlayer(ent)

    local ang = LocalPlayer():EyeAngles()
    local pos = LocalPlayer():EyePos()+ang:Forward()*10

            render.ClearStencil()
            render.SetStencilEnable(true)
            render.SetStencilReferenceValue( 1 )
            render.SetStencilWriteMask(1)
            render.SetStencilTestMask(1)
            render.SetStencilZFailOperation(STENCILOPERATION_REPLACE)
            render.SetStencilPassOperation(STENCILOPERATION_KEEP)
            render.SetStencilFailOperation(STENCILOPERATION_KEEP)
            render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_ALWAYS)


            render.SetBlend( 0 )
            /*
              local scale = Vector(1,1,0.9)
              local mat = Matrix()
              mat:Scale(Vector(scale))

              ent:EnableMatrix("RenderMultiply", mat)


              */
              local tran = Vector(0,0,0)
              local mat = Matrix()
              mat:Scale(Vector(1,1,1))
              mat:Translate( tran )
              ent:EnableMatrix("RenderMultiply", mat)

              ent:DrawModel()
              mat:Translate( Vector(0,0,0) )
              mat:Scale(Vector(1,1,1))
              ent:EnableMatrix("RenderMultiply", mat)
            render.SetBlend( 1 )
            render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
            cam.Start3D2D(Vector(pos.x, pos.y, pos.z),Angle(ang.p+90,ang.y,0),1)
              cam.IgnoreZ(false)
                surface.SetDrawColor(hud.customColor)
                surface.DrawRect(-ScrW(),-ScrH(),ScrW()*2,ScrH()*2)
                cam.IgnoreZ(false)
            cam.End3D2D()

              ent:DrawModel()
            render.SetStencilEnable(false)

end

-------------------------------------
-- Render player.
-------------------------------------
hook.Add("PostDrawOpaqueRenderables","PlayerStencil",function()
  for k, ply in pairs(player.GetAll()) do
    if ply != LocalPlayer() and indicators:GetBool() then
      drawPlayer( ply )
    end
  end
end)

-------------------------------------
-- Calculate the distance between two elements. http://www.purplemath.com/modules/distform.htm
-------------------------------------
function GetDistanceSqr(pp ,cx, cy)

    return (pp.x - cx) ^ 2 + (pp.y - cy) ^ 2
end

-------------------------------------
-- Find the  root.
-------------------------------------
function GetDistance(pp, cx, cy)
    return math.sqrt(GetDistanceSqr(pp, cx ,cy))
end

-------------------------------------
-- Indicator UI.
-------------------------------------
function hud.indicator_info()
    for k, v in pairs (player.GetAll()) do
      if v != LocalPlayer() then

        local    NameTag = (v:LocalToWorld(Vector(0,0,77))):ToScreen()
        local    Middle = (v:LocalToWorld(Vector(0,0,77/2))):ToScreen()
        local    speedTag = (v:LocalToWorld(Vector(0,0,10))):ToScreen()
        local    speed   = v:Speed()
        local playername = ""
        local      alpha = 255
        local     cx, cy = input.GetCursorPos()
        playername       = v:Name()




        alpha = Lerp(GetDistance(Middle, cx, cy)/(math.Clamp(alpha, 0, 255)/3) , 255, 0)


        if hudtoggle:GetBool() and indicators:GetBool() and v:Alive() then

          draw.DrawText(playername, "HUD Player", NameTag.x, NameTag.y-10 , Color(255,255,255, alpha), TEXT_ALIGN_CENTER)
          draw.DrawText(math.Round(speed/10)*10, "HUD Player", speedTag.x, speedTag.y+10, Color(255,255,255, alpha), TEXT_ALIGN_CENTER)
        end
      end
    end
end
hook.Add("HUDPaint", "Indicator info", hud.indicator_info)

function hud.indicator_offscreen()
  --[[
  for k, v in pairs (player.GetAll()) do
    if v != LocalPlayer() then
      local       plypos = (v:LocalToWorld(Vector(0,0,73/2))):ToScreen()
      local screenCenter = Vector(ScrW(), ScrH(), 0)/2

      plypos.x = plypos.x-screenCenter.x
      plypos.y = plypos.y-screenCenter.y

      local angle = math.deg(math.atan2(plypos.y, plypos.x))
      angle = math.rad(angle-90)
      local cos = math.cos(angle)
      local sin = -math.sin(angle)

      local m = cos/sin
      local screenBounds = screenCenter*0.8
      /*
      plypos.x= screenCenter.x+ (sin*150)
      plypos.y= screenCenter.y+ (cos*150)
      */
      if ScrW() > ScrH() then
        plypos.x= screenCenter.x+ (sin*(screenCenter.y/1.25))
        plypos.y= screenCenter.y+ (cos*(screenCenter.y/1.25))
      else
        plypos.x= screenCenter.x+ (sin*(screenCenter.x/1.5))
        plypos.y= screenCenter.y+ (cos*(screenCenter.x/1.5))
      end

      if cos >0 then
        plypos.x = screenBounds.y/m
        plypos.y = screenBounds.y
      else
        plypos.x = -screenBounds.y/m
        plypos.y = -screenBounds.y
      end

      if plypos.x >screenBounds.x then
        plypos.x = screenBounds.x
        plypos.y = screenBounds.x*m
      elseif plypos.x <-screenBounds.x then
        plypos.x = -screenBounds.x
        plypos.y = -screenBounds.x*(m)
      end
      plypos.x = plypos.x+screenCenter.x
      plypos.y = plypos.y+screenCenter.y

      surface.DrawLine( plypos.x, plypos.y, screenCenter.x, screenCenter.y )
      surface.SetMaterial(Material("materials/trick/triangle.png", "noclamp"))
      surface.DrawTexturedRectRotated( plypos.x, plypos.y, 15, 15, math.deg(-angle) )


    end
  end]]
end
hook.Add("HUDPaint", "Indicator offscreen", hud.indicator_offscreen)

-------------------------------------
-- Spectators UI.
-------------------------------------
function hud.spectators()
  local ply = LocalPlayer():GetObserverTarget() or LocalPlayer()
  if !Spectate.Players[ply] then return end
  if #Spectate.Players[ply]["Spectators"] > 0 then
    surface.SetFont( "HUD Spectators" )
    surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )

    local tx, ty = surface.GetTextSize( "Spectators")

    surface.SetTextPos( ScrW()-tx, 0)
    surface.DrawText( "Spectators" )

    for k, v in pairs(Spectate.Players[ply]["Spectators"]) do
      surface.SetFont( "HUD Spectators" )
      surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )

      local tx, ty = surface.GetTextSize( v:Nick() )

      surface.SetTextPos( ScrW()-tx, (k*(ty+1)))
      surface.DrawText( v:Nick() )
    end
  end
end
hook.Add("HUDPaint", "spectators", hud.spectators)

-------------------------------------
-- Hook Indicator.
-------------------------------------

hook.Add("PostDrawOpaqueRenderables","Hook_Indicator",function()
    local ang = LocalPlayer():EyeAngles()
    local pos = LocalPlayer():GrapplePos()
    local dist = LocalPlayer():GrapplePos():Distance(LocalPlayer():EyePos())
    local iconSize = math.Clamp( dist/5, 30, 100)
    local mat = Material( "materials/grpl/shapes/Chevron64.vmt" )
    local x, y = (iconSize/2), (iconSize/2)
    if !LocalPlayer():Grappling() then return end
    ang:RotateAroundAxis( ang:Right(), 90 )
      --ang:RotateAroundAxis( ang:Forward(), 0 )
    ang:RotateAroundAxis( ang:Up(), 140 )
    --render.DrawWireframeSphere( pos, LocalPlayer().ropeLength, 15, 15, Color(255,255,255), false )
    render.ClearStencil()
    render.SetStencilEnable(true)
      render.SetStencilReferenceValue( 1 )
      render.SetStencilWriteMask(1)
      render.SetStencilTestMask(1)
      render.SetStencilZFailOperation(STENCILOPERATION_REPLACE)
      render.SetStencilPassOperation(STENCILOPERATION_KEEP)
      render.SetStencilFailOperation(STENCILOPERATION_KEEP)
      render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_ALWAYS)
      render.SetBlend( 0 )
      cam.Start3D2D(pos,ang,0.5)

        surface.SetMaterial(mat)
        cam.IgnoreZ(true)
          surface.SetDrawColor(hud.customColor)

          --surface.DrawTexturedRect( (ScrW()/2)-2, (ScrH()/2)-2, 4, 4 )
          --surface.DrawTexturedRect(-iconSize,0,iconSize,iconSize)
          surface.DrawTexturedRectRotatedPoint(-17, 15, iconSize, iconSize, -140, 0, 0)
        cam.IgnoreZ(false)
      cam.End3D2D()
      render.SetBlend( 1 )
      /*
      render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_EQUAL)
      cam.Start3D2D(pos,ang,0.5)
        cam.IgnoreZ(true)
          surface.SetDrawColor(Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, hud.customColor.a-160))

          --surface.DrawTexturedRect( (ScrW()/2)-2, (ScrH()/2)-2, 4, 4 )
            surface.DrawTexturedRectRotatedPoint(-x, -y, iconSize, iconSize, -140, -x,-y)
        cam.IgnoreZ(false)
      cam.End3D2D()*/
    render.SetStencilEnable(false)
end)

function surface.DrawTexturedRectRotatedPoint( x, y, w, h, rot, x0, y0 )

	local c = math.cos( math.rad( rot ) )
	local s = math.sin( math.rad( rot ) )

	local newx = y0 * s - x0 * c
	local newy = y0 * c + x0 * s

	surface.DrawTexturedRectRotated( x + newx, y + newy, w, h, rot )

end

-------------------------------------
-- Crosshair UI.
-------------------------------------
worst = 0
function hud.crosshair()
  local ply       = LocalPlayer():GetObserverTarget() or LocalPlayer()
  local speed     = math.Round(ply:Speed()/10)*10
  local aimVec    = ply:GetAimVector()
  local pos       = ply:EyePos()
  local ang       = ply:EyeAngles()
  local grappling = ply:Grappling()
  local cG        = ply:CanGrapple()
  ang:RotateAroundAxis( ang:Right(), 90 )
  ang:RotateAroundAxis( ang:Up(), -90 )
  if ply != LocalPlayer() and IsFirstTimePredicted() then
    grabVar("Grappling", ply)
    grabVar("TracePos", ply)
  end

  if grappling then
    scale = scale and Lerp(0.075, scale, console.CmdVar("hud_crosshair_gRadius")) or console.CmdVar("hud_crosshair_gRadius") // 70
    chOpac = chOpac and Lerp(0.05, chOpac, console.CmdVar("hud_crosshair_opacity")) or console.CmdVar("hud_crosshair_opacity")
  else
    if cG then
      --surface.SetDrawColor( 0, 255, 0, 255 )
      scale = scale and Lerp(0.075, scale, console.CmdVar("hud_crosshair_hRadius")) or console.CmdVar("hud_crosshair_hRadius") // 110
      chOpac = chOpac and Lerp(0.05, chOpac, console.CmdVar("hud_crosshair_opacity")) or console.CmdVar("hud_crosshair_opacity")
    else
      --surface.SetDrawColor( hud.customColor )
      scale = scale and Lerp(0.075, scale, 0) or 0
      chOpac = chOpac and Lerp(0.05, chOpac, 0) or 0
    end
  end
  surface.SetDrawColor( hud.customColor )
  local w, h = scale, scale
  cam.Start3D( pos, ply:EyeAngles() )
    cam.IgnoreZ(true)
      cam.Start3D2D( pos+(ply:EyeAngles():Forward()*3.5), ang, 0.001 )
        local x, y = -(w/2), -(h/2)
        local rad = 0.7
        // Dot
        surface.SetMaterial(Material("materials/grpl/shapes/Dot.vmt"))
        surface.DrawTexturedRect( -25/2, -25/2, 25, 25 )
        surface.SetDrawColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, chOpac) )
        //Circle
        surface.SetMaterial(Material("materials/grpl/shapes/Circle32.vmt"))
        surface.DrawTexturedRect( x, y, w, h )

        surface.SetMaterial(Material("materials/grpl/shapes/chevron32.vmt"))

        if grappling then


        elseif !grappling && cG then
          local c = math.cos( math.rad(35) ) * rad
        	local s = math.sin( math.rad(35) ) * rad
          w, h = w-40, h+10
          x, y = -(w)-40, -(h)
          x, y = c*x, s*y
          surface.DrawTexturedRectRotated(x, y, w, h, -35)

          c = math.cos( math.rad(145) ) * rad
        	s = math.sin( math.rad(145) ) * rad
          x, y = -(w)-40, -(h)
          x, y = c*x, s*y
          surface.DrawTexturedRectRotated( x, y, w, h, -145)

          c = math.cos( math.rad(-90) ) * rad
        	s = math.sin( math.rad(-90) ) * rad
          x, y = -(w), -(h)+10
          x, y = c*x, s*y
          surface.DrawTexturedRectRotated( x, y, w, h, 90)
        end

      cam.End3D2D()
    cam.IgnoreZ(false)
  cam.End3D()


  /* -- Wall detecting crosshair
  local trace = {}
	trace.start = pos
	trace.endpos = pos+(aimVec * 2000)
	trace.filter = ply
	local tr = util.TraceLine(trace)
  if ply:CanGrapple() then
    hitLoc = tr.HitPos
  end

  local ang = tr.HitNormal:Angle() --ply:EyeAngles()

  ang:RotateAroundAxis( ang:Right(), -90 )
  --ang:RotateAroundAxis( ang:Up(), -90 )
  cam.Start3D( pos, ply:EyeAngles() )
    cam.IgnoreZ(true)
      cam.Start3D2D( hitLoc, ang, 0.1 )
        local sens = 0.4
        dist = dist and Lerp(0.1, dist, pos:Distance(tr.HitPos)*sens ) or pos:Distance(tr.HitPos)*sens

        local w, h = math.Clamp(dist, 100, 1500),math.Clamp(dist, 100, 1500)
        local x, y = (-w/2), (-h/2)
        surface.SetMaterial(Material("materials/grpl/Circle64.png", "smooth noclamp"))
        surface.DrawTexturedRectRotatedPoint( x, y, w, h, -90, x, y )
        surface.SetMaterial(Material("materials/grpl/chevron64.png", "smooth noclamp"))
        surface.DrawTexturedRectRotatedPoint( x, h , w-5, h, 45, x, y )

      cam.End3D2D()
    cam.IgnoreZ(false)
  cam.End3D()*/
  /*
  if hudtoggle:GetBool() and crosshairtoggle:GetInt() > 0 then
    if ply:CanGrapple() then
      surface.SetDrawColor( 0, 255, 0, 255 )
    else
      surface.SetDrawColor( hud.customColor )
    end

    surface.SetMaterial(Material("materials/trick/hud/Dot.png", "noclamp"))
    surface.DrawTexturedRect( (ScrW()/2)-2, (ScrH()/2)-2, 4, 4 )
    surface.SetDrawColor( 255, 1, 1, 255 )
    --surface.DrawCircle( ScrW()/2, ScrH()/2, 4, 255, 255, 255, 255 )
    surface.SetFont( "HUD Player" )
    if crosshairtoggle:GetInt() == 1 then
      local width, height = surface.GetTextSize(math.Round(speed/10)*10 )
      local centerW, centerH = (ScrW()/2)+1, (ScrH()/2)+(height/4)
      draw.DrawText(math.Round(speed/10)*10, "HUD Player", centerW, centerH, Color(255,255,255, 255), TEXT_ALIGN_CENTER)
    end
  end*/

end
hook.Add("PostDrawOpaqueRenderables", "ch", hud.crosshair)
--hook.Add("HUDPaint", "ch", hud.crosshair)

-------------------------------------
-- Draws a outlined box.
-------------------------------------
function hud.OutlinedBox( x, y, w, h, thickness, color)
    surface.SetDrawColor( color )

    for i=0, thickness  do

        surface.DrawRect(x, y, w, h-h+thickness)
        surface.DrawRect(x, y+h-thickness, w, h-h+thickness)
        surface.DrawRect(x, y, w-w+thickness, h)
        surface.DrawRect(x+w-thickness+.5, y, w-w+thickness, h)
    end

end

-------------------------------------
-- Draws a box.
-------------------------------------
function hud.drawBox(corner, x, y, w, h, color, key, font, txtcolor, enum, keoutline, thickness, outlinecolor)
  local ply = LocalPlayer():GetObserverTarget() or LocalPlayer()
  surface.SetFont( font )
  local width, height = surface.GetTextSize( key )
  centerW, centerH = x + ( w/2 ), (y + (h/2))-(height/2)

  if  ( keyEnum:KeyDown(ply, enum ) and keoutline == true and outlineecho:GetBool()) then
    --surface.DrawRect(x, y, w, h) maybe
    hud.OutlinedBox(x, y, w, h, thickness, outlinecolor)
  end
  if keyEnum:KeyDown(ply, enum ) and !outlineecho:GetBool() then
    color.a = color.a and Lerp(0.1, color.a, 230) or 230
  else
    color.a = color.a and Lerp(0.1, color.a, hud.color.a) or hud.color.a
  end

  draw.RoundedBox(corner, x, y, w, h, color)
  draw.DrawText( key, font, centerW, centerH, txtcolor, TEXT_ALIGN_CENTER )
end

-------------------------------------
-- KeyEcho UI.
-------------------------------------
function hud.keyEcho()

  if !hudtoggle:GetBool() then return end
    local ply      = LocalPlayer():GetObserverTarget() or LocalPlayer()
    if console.CmdVar("hud_keyecho") == 2 then

      surface.SetFont( "HUD Echo2" )
      if keyEnum:KeyDown(ply, IN_FORWARD ) then

        local width, height = surface.GetTextSize( "˄" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("˄", "HUD Echo2", centerW, centerH-30, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_BACK ) then
        local width, height = surface.GetTextSize( "˅" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("˅", "HUD Echo2", centerW, centerH+30, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_MOVERIGHT ) then
        local width, height = surface.GetTextSize( ">" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText(">", "HUD Echo2", centerW+30, centerH, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_MOVELEFT ) then
        local width, height = surface.GetTextSize( "<" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("<", "HUD Echo2", centerW-30, centerH, hud.customColor, TEXT_ALIGN_CENTER)
      end

        surface.SetFont( "HUD Echo2txt" )
      if keyEnum:KeyDown(ply, IN_JUMP ) then

        local width, height = surface.GetTextSize( "J" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("J", "HUD Echo2txt", centerW+30, centerH+30, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_DUCK ) then
        local width, height = surface.GetTextSize( "D" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("D", "HUD Echo2txt", centerW-30, centerH+30, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_ATTACK ) then
        local width, height = surface.GetTextSize( "M1" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("M1", "HUD Echo2txt", centerW-30, centerH-30, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_ATTACK2 ) then
        local width, height = surface.GetTextSize( "M2" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("M2", "HUD Echo2txt", centerW+32, centerH-30, hud.customColor, TEXT_ALIGN_CENTER)
      end
    end


end

hook.Add("HUDPaint", "kE", hud.keyEcho)

function keyEchoes:Draw3D(ply)
  /*
  if !hudtoggle:GetBool() then return end
  if console.CmdVar("hud_keyecho") == 1 then
    local thickness = 7
    local w, h = 110, 110
    for k, v in pairs(hud.keys) do
      local x, y = -50, 350
      if k == "A" then
        x, y = x-(w+5),y+(h+5)
      elseif k == "S" then
        x, y = x,y+(h+5)
      elseif k == "D" then
        x, y = x+(w+5),y+(h+5)
      elseif k == "JUMP" then
        x, y = x,y+((h*2)+10), (w*2)+5
      elseif k == "DUCK" then
        x, y = x-(w+5),y+((h*2)+10)
      elseif k == "M1" then
        x, y = x-(w+5),y
      elseif k == "M2" then
        x, y = x+(w+5),y
      end
      hud.drawBox(1, x,y, w, h, v.color, k, "HUD 3dEcho", hud.customColor, v.enum, true, thickness, hud.customColor)
    end
  end
*/
end

function CreateGraph( tbl, x, y, width, height, vel, outlineWidth, scrollspeed, outline )
  table.Empty( tbl.Poly )        -- Reset the poly table
  table.Empty( tbl.polyOutline ) -- Reset the poly table
  if tbl ~= nil then
    if #tbl.Points == 0 then -- Fill the table to prevent the graph from standing still in the beginning
      for i = 1, ( width*scrollspeed ) do
        table.insert( tbl.Points, 0 )
      end
    end

    if math.Round((vel*10)/10)*10 >= tbl.Points[#tbl.Points] then
      table.insert( tbl.Points, Lerp( 0.005, tbl.Points[#tbl.Points],math.Round((vel*10)/100)*100 ))  -- Insert a new point
    else -- else if lower than previous point
      table.insert( tbl.Points, Lerp( 0.005, tbl.Points[#tbl.Points],math.Round((vel*10)/100)*100 ))
    end
    if #tbl.Points > ( width*scrollspeed ) then -- remove if the point has reached the end
        table.remove( tbl.Points, 1 )
    end

  end

  -- Have not tested this outside my own 3d hud module, but positive numbers go down in y axis
  table.insert( tbl.Poly, { x = ( x+width/2 ), y = y+height*10 } )         -- This makes it so the graph is drawn from the bottom, but that part is hidden with stencil
  table.insert( tbl.polyOutline, { x = ( x+width/2 ), y = y+height*10 } )

  -- Draw the graph
  for k, v in pairs( tbl.Points ) do
    local barh = math.Round( ( ( v/tbl.topGraph ) )*( height/10 ), 1 )
    table.insert( tbl.Poly, { x = ( x+1 ) + ( k/scrollspeed ), y = y+height-barh, hi = barh } )
    if outline then
      table.insert( tbl.polyOutline, { x = x + ( k/scrollspeed ), y = y+height-barh-1-outlineWidth, hi = barh } )
      table.insert( tbl.polyOutline, { x = ( x+outlineWidth ) + ( k/scrollspeed ), y = y+height-barh-1, hi = barh } )
    end
    if k == 1 then
      table.insert( tbl.Poly, { x = x, y = y+height } ) -- Fills the first poly
      if outline then
        table.insert( tbl.polyOutline, { x = x, y = y+height } )
      end
    end
  end
  table.insert( tbl.Poly, { x = x+width, y = y+height } ) -- Fills the rest of the graph
  table.insert( tbl.polyOutline, { x = x+width, y = y+height } )
end

function draw.Circle( x, y, radius, seg, deg, rot, color )
	local cir = {}
  local c = math.cos( math.rad( rot ) )
	local s = math.sin( math.rad( rot ) )

	table.insert( cir, { x = x, y = y, u = 0.5, v = 0.5 } )
	for i = 0, seg do
		local a = math.rad( rot + ( i / seg ) * -deg )
		table.insert( cir, { x = x + math.sin( a ) * radius, y = y + math.cos( a ) * radius, u = math.sin( a ) / 2 + 0.5, v = math.cos( a ) / 2 + 0.5 } )
	end

	local a = math.rad( rot ) -- This is needed for non absolute segment counts
	table.insert( cir, { x = x + math.sin( a ) * radius, y = y + math.cos( a ) * radius, u = math.sin( a ) / 2 + 0.5, v = math.cos( a ) / 2 + 0.5 } )
  surface.SetDrawColor( color.r, color.g, color.b, color.a )
	draw.NoTexture()
	surface.DrawPoly( cir )
end

function draw.SoM(x, y, rad, rot, gear, alpha, active)
    render.ClearStencil()
    render.SetStencilEnable( true )
    render.SetStencilWriteMask( 255 )
    render.SetStencilTestMask( 255 )

    -- The graph fill
    render.SetStencilReferenceValue( 10 )

      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
      render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
      render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
      render.SetStencilPassOperation( STENCILOPERATION_KEEP )
      draw.Circle( x, y, rad-9, 50, 360, rot, Color(255,255,255,alpha) )
      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_GREATER )
      render.SetStencilFailOperation( STENCILOPERATION_KEEP )
      draw.Circle( x, y, rad, 50, 90, rot, Color(255,255,255,alpha) )
    render.SetStencilEnable(false)

    //Cur Gear
    local a = math.rad( rot+8 )
    local tx, ty = surface.GetTextSize( gear )


    surface.SetFont( "HUD Gear" )
    surface.SetTextColor( Color(255,255,255,alpha) )
    surface.SetTextPos( x+(math.sin( a )*(rad-5))-(tx/2), y+(math.cos( a )*(rad))-(ty/2)  )
    surface.DrawText( gear )
    a = math.rad( rot )
    surface.DrawTexturedRectRotated( x+(math.sin( a )*(rad-5)), y+(math.cos( a )*(rad)), 40, 5, rot-90 )

    //Next gear
    a = math.rad( (rot-8) + 1 * -90  )
    tx, ty = surface.GetTextSize( gear+1  )


    surface.SetTextPos( x+(math.sin( a )*(rad))-(tx/2), y+(math.cos( a )*(rad-5))-(ty/2) )
    surface.DrawText( gear+1 )
    a = math.rad( rot + 1 * -90 )
    surface.DrawTexturedRectRotated( x+(math.sin( a )*(rad)), y+(math.cos( a )*(rad-5)), 40, 5, rot )

    /*
    for i=0, 10 do
      local a = math.rad( rot + ( i / 10 ) * -90 )

      surface.DrawTexturedRectRotated( x+ math.sin( a )*(rad-5), y+ math.cos( a )*(rad-5), 40, 5, 90/ )
    end*/
end

function hud.keyEchoes( ply, x, y, w, h, thickness)
  //KeyEcho
  local kE_w, kE_h = w, h
  local V_ROW_1, V_ROW_2, V_ROW_3 = y-kE_h+thickness, y-(kE_h*2), y-(kE_h*3)-thickness
  local H_ROW_1, H_ROW_2, H_ROW_3 = x+thickness, x+((kE_w))+(thickness*2), x+((kE_w*2))+(thickness*3)

  if console.CmdVar("hud_keyecho") == 1 then
    kE_TOP = kE_TOP and math.Round(UT:Hermite(0.1, kE_TOP, (kE_h*3)+(thickness*2)),2) or 0
    kE_MID_2_U = kE_MID_2_U and math.Round(UT:Hermite(0.1, kE_MID_2_U, (kE_h*2)+(thickness*2)),2) or -kE_h
    if kE_TOP >= (kE_h) then
      kE_ROW_1 = kE_ROW_1 and math.Round(UT:Hermite(1, kE_ROW_1, kE_h*1),2) or 0
    end
    if kE_TOP >= (kE_h*2)+thickness then
      kE_ROW_2 = kE_ROW_2 and math.Round(UT:Hermite(1, kE_ROW_2, (kE_h*2)+thickness),2) or 0
    end
    if kE_TOP >= (kE_h/1.5) then
      kE_DUCK_A = math.Round(UT:Hermite(0.1, kE_DUCK_A, 255),2)
      kE_JUMP_A = math.Round(UT:Hermite(0.1, kE_JUMP_A, 255),2)
    end

    if kE_TOP >= (kE_h)+(kE_h/1.5) then
      kE_A_A = math.Round(UT:Hermite(0.1, kE_A_A, 255),2)
      kE_S_A = math.Round(UT:Hermite(0.1, kE_S_A, 255),2)
      kE_D_A = math.Round(UT:Hermite(0.1, kE_D_A, 255),2)
    end
    if kE_TOP >= (kE_h*2)+(kE_h/1.5) then
      kE_W_A = math.Round(UT:Hermite(0.1, kE_W_A, 255),2)
      kE_M1_A = math.Round(UT:Hermite(0.1, kE_M1_A, 255),2)
      kE_M2_A = math.Round(UT:Hermite(0.1, kE_M2_A, 255),2)
    end

    if console.CmdVar("hud_keyecho_press") == 0 then
      for k, v in pairs(keyEnum.keys) do
        if math.Round(kE_TOP) == math.Round((kE_h*3)+(thickness*2)) then
          if !kE_Flash then
            kE_W_PRESS = 255
            kE_A_PRESS = 255
            kE_S_PRESS = 255
            kE_D_PRESS = 255
            kE_M1_PRESS = 255
            kE_M2_PRESS = 255
            kE_DUCK_PRESS = 255
            kE_JUMP_PRESS = 255
            kE_Flash = true
          end
          if k == "W" then
            kE_W_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_W_PRESS, 100) or UT:Hermite(0.25, kE_W_PRESS, 0)
          elseif k == "A" then
            kE_A_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_A_PRESS, 100) or UT:Hermite(0.25, kE_A_PRESS, 0)
          elseif k == "S" then
            kE_S_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_S_PRESS, 100) or UT:Hermite(0.25, kE_S_PRESS, 0)
          elseif k == "D" then
            kE_D_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_D_PRESS, 100) or UT:Hermite(0.25, kE_D_PRESS, 0)
          elseif k == "M1" then
            kE_M1_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_M1_PRESS, 100) or UT:Hermite(0.25, kE_M1_PRESS, 0)
          elseif k == "M2" then
            kE_M2_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_M2_PRESS, 100) or UT:Hermite(0.25, kE_M2_PRESS, 0)
          elseif k == "DUCK" then
            kE_DUCK_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_DUCK_PRESS, 100) or UT:Hermite(0.25, kE_DUCK_PRESS, 0)
          elseif k == "JUMP" then
            kE_JUMP_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_JUMP_PRESS, 100) or UT:Hermite(0.25, kE_JUMP_PRESS, 0)
          end
        end
      end
      local col = hud.customColor2
      // M1
      surface.SetDrawColor( Color(col.r, col.g, col.b, kE_M1_PRESS) )
      surface.DrawRect(H_ROW_1, V_ROW_3, kE_w, kE_h)
      // W
      surface.SetDrawColor( Color(col.r, col.g, col.b, kE_W_PRESS) )
      surface.DrawRect(H_ROW_2, V_ROW_3, kE_w, kE_h)
      // M2
      surface.SetDrawColor( Color(col.r, col.g, col.b, kE_M2_PRESS) )
      surface.DrawRect(H_ROW_3, V_ROW_3, kE_w, kE_h)
      // A
      surface.SetDrawColor( Color(col.r, col.g, col.b, kE_A_PRESS) )
      surface.DrawRect(H_ROW_1, V_ROW_2, kE_w, kE_h)
      // S
      surface.SetDrawColor( Color(col.r, col.g, col.b, kE_S_PRESS) )
      surface.DrawRect(H_ROW_2, V_ROW_2, kE_w, kE_h)
      // D
      surface.SetDrawColor( Color(col.r, col.g, col.b, kE_D_PRESS) )
      surface.DrawRect(H_ROW_3, V_ROW_2, kE_w, kE_h)
      // DUCK
      surface.SetDrawColor( Color(col.r, col.g, col.b, kE_DUCK_PRESS) )
      surface.DrawRect(H_ROW_1, V_ROW_1, kE_w, kE_h)
      // JUMP
      surface.SetDrawColor( Color(col.r, col.g, col.b, kE_JUMP_PRESS) )
      surface.DrawRect(H_ROW_2, V_ROW_1, kE_w*2+thickness, kE_h)
    elseif console.CmdVar("hud_keyecho_press") == 1 then

    end
  elseif console.CmdVar("hud_keyecho") == 0 then
    kE_TOP = kE_TOP and math.Round(UT:Hermite(0.1, kE_TOP, 0),2) or 0
    kE_MID_2_U = kE_MID_2_U and math.Round(UT:Hermite(0.1, kE_MID_2_U, -kE_h),2) or -kE_h
    if kE_TOP <= (kE_h) then
      kE_ROW_1 = kE_ROW_1 and math.Round(UT:Hermite(1, kE_ROW_1, 0),2) or 0
    end
    if kE_TOP <= (kE_h*2)+thickness then
      kE_ROW_2 = kE_ROW_2 and math.Round(UT:Hermite(1, kE_ROW_2, 0),2) or 0
    end
    if kE_TOP <= (kE_h) then
      kE_DUCK_A = math.Round(UT:Hermite(0.25, kE_DUCK_A, 0),2)
      kE_JUMP_A = math.Round(UT:Hermite(0.25, kE_JUMP_A, 0),2)
    end
    if kE_TOP <= (kE_h)+(kE_h) then
      kE_A_A = math.Round(UT:Hermite(0.25, kE_A_A, 0),2)
      kE_S_A = math.Round(UT:Hermite(0.25, kE_S_A, 0),2)
      kE_D_A = math.Round(UT:Hermite(0.25, kE_D_A, 0),2)
    end
    if kE_TOP <= (kE_h*2)+(kE_h) then
      kE_W_A = math.Round(UT:Hermite(0.25, kE_W_A, 0),2)
      kE_M1_A = math.Round(UT:Hermite(0.25, kE_M1_A, 0),2)
      kE_M2_A = math.Round(UT:Hermite(0.25, kE_M2_A, 0),2)
    end
    if kE_Flash then
      kE_W_PRESS = UT:Hermite(0.25, kE_W_PRESS, 0)
      kE_A_PRESS = UT:Hermite(0.25, kE_A_PRESS, 0)
      kE_S_PRESS = UT:Hermite(0.25, kE_S_PRESS, 0)
      kE_D_PRESS = UT:Hermite(0.25, kE_D_PRESS, 0)
      kE_M1_PRESS = UT:Hermite(0.25, kE_M1_PRESS, 0)
      kE_M2_PRESS = UT:Hermite(0.25, kE_M2_PRESS, 0)
      kE_DUCK_PRESS = UT:Hermite(0.25, kE_DUCK_PRESS, 0)
      kE_JUMP_PRESS = UT:Hermite(0.25, kE_JUMP_PRESS, 0)
      kE_Flash = false
    end
  end
  surface.SetDrawColor( hud.color )
  if kE_TOP > 0 then
    --surface.DrawRect(x, y-thickness/2, (kE_w*3), thickness)                     -- BOTTOM
    surface.DrawRect(x, y-kE_TOP, (kE_w*3)+(thickness*3), thickness)              -- TOP
    surface.DrawRect(x, y-kE_ROW_1, (kE_w*3)+(thickness*3), thickness)            -- ROW_1
    surface.DrawRect(x, y-kE_ROW_2, (kE_w*3)+(thickness*3), thickness)            -- ROW_2
    surface.DrawRect(x, y, thickness, -kE_TOP)                                    -- LEFT
    surface.DrawRect(x+(kE_w*3)+(thickness*3), y, thickness, -kE_TOP)             -- RIGHT
    surface.DrawRect(x+kE_w+thickness, y, thickness, -kE_TOP)                     -- MID_1 Up
    if kE_TOP >= (kE_h) then
      surface.DrawRect(x+(kE_w*2)+(thickness*2), y-kE_h, thickness, -kE_MID_2_U)  -- MID_2 Up
    end
  end

  SetFont("HUD Echo", {
    font = "Ostrich Sans",
    weight = 700,
    size = (kE_w/2.5),
    antialias = true,

  })

  local kE_keyW, kE_keyH = surface.GetTextSize( "W" )
  local kE_keyCW, kE_keyCH = (H_ROW_2+(kE_w/2))-(kE_keyW/2), (V_ROW_3+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_W_A) )
  surface.DrawText( "W" )

  kE_keyW, kE_keyH = surface.GetTextSize( "M1" )
  kE_keyCW, kE_keyCH = (H_ROW_1+(kE_w/2))-(kE_keyW/2), (V_ROW_3+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_M1_A) )
  surface.DrawText( "M1" )

  kE_keyW, kE_keyH = surface.GetTextSize( "M2" )
  kE_keyCW, kE_keyCH = (H_ROW_3+(kE_w/2))-(kE_keyW/2), (V_ROW_3+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_M2_A) )
  surface.DrawText( "M2" )

  kE_keyW, kE_keyH = surface.GetTextSize( "S" )
  kE_keyCW, kE_keyCH = (H_ROW_2+(kE_w/2))-(kE_keyW/2), (V_ROW_2+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_S_A) )
  surface.DrawText( "S" )

  kE_keyW, kE_keyH = surface.GetTextSize( "A" )
  kE_keyCW, kE_keyCH = (H_ROW_1+(kE_w/2))-(kE_keyW/2), (V_ROW_2+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_A_A) )
  surface.DrawText( "A" )

  kE_keyW, kE_keyH = surface.GetTextSize( "D" )
  kE_keyCW, kE_keyCH = (H_ROW_3+(kE_w/2))-(kE_keyW/2), (V_ROW_2+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_D_A) )
  surface.DrawText( "D" )

  kE_keyW, kE_keyH = surface.GetTextSize( "DUCK" )
  kE_keyCW, kE_keyCH = (H_ROW_1+(kE_w/2))-(kE_keyW/2), (V_ROW_1+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_DUCK_A) )
  surface.DrawText( "DUCK" )

  kE_keyW, kE_keyH = surface.GetTextSize( "JUMP" )
  kE_keyCW, kE_keyCH = (H_ROW_2+((kE_w*2)/2))-(kE_keyW/2), (V_ROW_1+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_JUMP_A) )
  surface.DrawText( "JUMP" )

end

-------------------------------------
-- Left Side UI.
-------------------------------------
function leftSide:Draw3D(ply, w, h)
  local localPly = ply
  if ply:GetObserverTarget() then
    ply = ply:GetObserverTarget()
  end
  local x, y      = -ScrW()-100, 640      // X and Y position
  local w, h      = ScrW(), 5            // Width and Height
  local hp         = math.Clamp(ply:Health(), 0, 100)
  local hpPos      = x+w
  local hpWidth     = 500
  local hpHeight    = 15
  // Default line
  surface.SetDrawColor( hud.color )
  surface.DrawRect( x, y, w, h )
  local healthWidth    = hp*(hpWidth/100)
  hpBar = hpBar and UT:Hermite(0.05, hpBar, healthWidth) or healthWidth
  surface.SetDrawColor( hud.customColor )
  surface.DrawRect( hpPos, y+(h/2)-(hpHeight/2), -hpBar, hpHeight )

  SetFont("HUD Speed", {
    font = "Ostrich Sans",
    weight = 600,
    size = 70,
    antialias = false
  })
  surface.SetTextColor( hud.customColor )
  local hpX, hpY = surface.GetTextSize( math.Round( hp ) )
  surface.SetTextPos( hpPos-(hpBar/2)-(hpX/2),  y+10)
  surface.DrawText( math.Round( hp ) )

  //End of HP
  surface.SetDrawColor( hud.color )
  surface.DrawRect( hpPos, y+(h/2)-((hpHeight+5)/2), 3, hpHeight+5 )
  surface.DrawRect( hpPos-hpWidth, y+(h/2)-((hpHeight+5)/2), 3, hpHeight+5 )
end

-------------------------------------
-- Right Side UI.
-------------------------------------
function rightSide:Draw3D(ply, w, h)
  local localPly = ply
  if ply:GetObserverTarget() then
    ply = ply:GetObserverTarget()
  end
  local x, y        = 100, 640      // X and Y position
  local w, h        = ScrW(), 5            // Width and Height
  local vel         = ply:Speed2D()
  local velPos      = x
  local vBWidth     = 500
  local vBHeight    = 15
  local kEPos       = x+w-(95*3)-(h*4)
  local kE_w, kE_h = 95, 95
  // Default line
  surface.SetDrawColor( hud.color )
  surface.DrawRect( x, y, w, h )

  //Speed
  /*
  if localPly:TSS() and localPly:TSS() > 100 then -- If topspeed exists
    velMult = velMult and Lerp( 0.01, velMult, velMaxWidth/(math.Round( localPly:TSS()/50 )*50) ) or 1
  else
    velMult =1
  end*/

  if velo:GetInt() == 1 then
    vel = ply:Speed()
  end

  local maxVelWidth = (vBWidth)/math.Clamp(localPly:TSS(), 100, localPly:TSS())
  local velWidth    = math.Clamp(vel,0, localPly:TSS())*maxVelWidth
  local hundVel = math.floor((math.floor( vel/50 )*50)/100)
  velBar = velWidth or Lerp(0.25, velBar, velWidth) and velBar
  surface.SetDrawColor( hud.customColor )
  surface.DrawRect( velPos, y+(h/2)-(vBHeight/2), velBar, vBHeight )
  //End of Vel
  surface.SetDrawColor( hud.color )
  surface.DrawRect( velPos, y+(h/2)-((vBHeight+5)/2), 3, vBHeight+5 )
  surface.DrawRect( velPos+vBWidth, y+(h/2)-((vBHeight+5)/2), 3, vBHeight+5 )

  SetFont("HUD Speed", {
    font = "Ostrich Sans",
    weight = 600,
    size = 70,
    antialias = false
  })
  surface.SetTextColor( hud.customColor )
  local velX, velY = surface.GetTextSize( math.Round( vel ) )
  surface.SetTextPos( velPos+(velBar/2)-(velX/2),  y+10)
  surface.DrawText( math.Round( vel ) )

  hud.keyEchoes(ply, kEPos, y, kE_w, kE_h, h)
end

-------------------------------------
-- Top UI.
-------------------------------------
function top:Draw3D(ply, w, h)
  local x, y      = -ScrW()-100, -770      // X and Y position
  local w, h      = ScrW()*2+100, 5            // Width and Height
  local localPly = ply
  if ply:GetObserverTarget() then
    ply = ply:GetObserverTarget()
  end
  // Default line
  surface.SetDrawColor( hud.customColor )
  surface.DrawRect( x, y, w, h )
end
--[=[
-------------------------------------
-- Speedometer UI.
-------------------------------------
SpeedoMeter = {}

local blur = Material("pp/blurscreen")
function speedDisplay:Draw3D(ply, w, h)
  local x      = -910       -- x position of the panel
  local y      = 310        -- y position of the panel
  local localPly = ply
  local plyLobby = lobby:getPlayerLobby(ply)
  /*
  if plyLobby != "None" then
    hud.customColor = plyLobby["minigameColors"][plyLobby["minigamePlayers"][ply]]
  else
    hud.customColor = Color(255,255,255)
  end*/
  if ply:GetObserverTarget() then
    ply = ply:GetObserverTarget()
  end
  local vel    = ply:Speed()
  local height = 70                           -- height of the panel
  local width  = 200                          -- width of the panel

  if !hudtoggle:GetBool() then return end
  local scrollspeed = 4                       -- How fast the graph scrolls, higher = slower
  local topGraph = 100                        -- Same as self.topGraph Decides how many units/10 you can fit in the the graph
  local fillCol = Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, 200 )      -- fill color

  if sGraph:GetBool() then

  	--surface.DrawTexturedRect( 25, 25, 100, 100 )
    /*
    local gear = 1
    if vel < 200 then
      curGear = 1
    elseif vel >= 200 then
      curGear = 2
    elseif vel >= 300 then
      curGear = 3
    elseif vel >= 400 then
      curGear = 4
    elseif vel >= 500 then
      curGear = 5
    end
    x = x+150
    if !spinning or math.Round(spinning) >= 360 then
      spinning = 0
    end
    spinning = Lerp( 0.0005, math.Round(spinning, 1), 360*2 ) -- Slowly increase graph height

    draw.SoM(x, y, 200, -90, 1, 255, curGear==1)
    draw.SoM(x, y, 175, -205, 2, 20, curGear==2)
    draw.SoM(x, y, 150, -310, 3, 15, curGear==3)
    draw.SoM(x, y, 125, -70, 4, 10, curGear==4)
    draw.SoM(x, y, 100, -190, 5, 5, curGear==5)*/
    --
    if !SpeedoMeter.Points then
      SpeedoMeter.Points = {}                    -- All the points in the graph which stores speed per point.
      SpeedoMeter.Poly = {}                      -- The graph poly
      SpeedoMeter.polyOutline = {}               -- Graph poly outline
      SpeedoMeter.topGraph = 100                 -- The number equals the speed which is required to reach the top of the graph
    end

    if localPly:TSS() and localPly:TSS() > 100 then -- If topspeed exists
      SpeedoMeter.topGraph = Lerp( 0.01, SpeedoMeter.topGraph, math.Round( localPly:TSS()/50 )*50 ) -- Slowly increase graph height
    end

    if velo:GetInt() == 1 then
      vel = ply:Speed2D()
    end

    CreateGraph( SpeedoMeter, x, y, width, height, vel, outlineWidth, scrollspeed, outline )

    // Graph Background
    surface.SetDrawColor( hud.color )
    surface.DrawRect( x, y, width, height )

    -- Stencil start
    render.ClearStencil()
    render.SetStencilEnable( true )
    render.SetStencilWriteMask( 255 )
    render.SetStencilTestMask( 255 )

    -- The graph fill
    render.SetStencilReferenceValue( 10 )


      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
      render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
      render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
      render.SetStencilPassOperation( STENCILOPERATION_KEEP )

    render.SetBlend( 0 )

      surface.SetDrawColor( Color(255,0,0,255) )
      draw.NoTexture()
      surface.DrawPoly( SpeedoMeter.Poly )


    render.SetBlend( 1 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
    render.SetStencilFailOperation( STENCILOPERATION_KEEP )

    --surface.DrawRect( x, y, width, height )
    /* -- This turned out to be some cool looking waveform
    local i = 1
    local gradients = 20
    for i = 1, gradients do
      surface.DrawRect( x+i*gradients, y, width/gradients, height )
    end*/
    /*
    local i = 1
    local smooth = 100
    local normalAlpha = 255
    local rate = normalAlpha/(smooth/6) //Full alpha at about 1/6th of the way

    for i = 1, smooth do
      if i <= smooth/2 then
        surface.SetDrawColor( Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, math.Clamp(rate*i, 0, normalAlpha ) ) )
      else

        surface.SetDrawColor( Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, math.Clamp(rate*(smooth-i), 0, normalAlpha ) ) )
      end
      surface.DrawRect( x+((width/smooth)*(i)), y, width/smooth, height )
    end*/

    local normalAlpha = 120
    local segments = 1
    local smooth = 20


    for i = 1, smooth, segments do
      local alpha = 0
      local rate = normalAlpha/(width/(smooth*segments))
      local length = math.Clamp(width-(i*2), 0, width-(i*2))

      surface.SetDrawColor( Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, rate) )
      surface.DrawRect( x+i  , y, length, height )
    end

    render.SetStencilReferenceValue( 11 )
    render.SetStencilWriteMask( 255  )
    render.SetStencilTestMask( 255 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
    render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
    render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilPassOperation( STENCILOPERATION_KEEP )

    render.SetBlend( 0 )

      surface.SetDrawColor( Color(255,0,0,255) )
      draw.NoTexture()
      surface.DrawPoly( SpeedoMeter.Poly )


    render.SetBlend( 1 )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_GREATER )


    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )
    local tx, ty = surface.GetTextSize( math.Round( vel ) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round( vel ) )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )

    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( hud.customColor2 )
    local tx, ty = surface.GetTextSize( math.Round( vel ) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round( vel ) )
    /*
    render.SetStencilReferenceValue( 11 )
    render.SetStencilWriteMask( 255 )
    render.SetStencilTestMask( 255 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
    render.SetStencilFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilZFailOperation( STENCILOPERATION_REPLACE )
    render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( Color(hud.customColor.r-225, hud.customColor.g-225, hud.customColor.b-225, 255) )
    local tx, ty = surface.GetTextSize( math.Round( vel ) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round( vel ) )*/

  render.SetStencilEnable(false)


else
    // Graph Background
    surface.SetDrawColor( hud.color )
    surface.DrawRect( x, y, width, height )
    surface.SetDrawColor( hud.customColor )
    surface.DrawRect( x, y+height, width, 1 )


        -- Stencil start
    render.ClearStencil()
    render.SetStencilEnable( true )
    render.SetStencilWriteMask( 255 )
    render.SetStencilTestMask( 255 )

    -- The graph fill
    render.SetStencilReferenceValue( 10 )


    render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilPassOperation( STENCILOPERATION_KEEP )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )
    render.SetStencilFailOperation( STENCILOPERATION_REPLACE )


    local normalAlpha = 120
    local segments = 1
    local smooth = 20

    local maxVelWidth = (width)/math.Clamp(localPly:TSS(), 100, localPly:TSS())
    local velWidth    = math.Clamp(vel,0, localPly:TSS())*maxVelWidth
    velBar = velWidth or Lerp(0.25, velBar, velWidth) and velBar
    --velAlpha = velAlpha and Lerp(0.25, velBar, velWidth) or velWidth

    for i = 1, smooth, segments do
      local alpha = 0
      local rate = normalAlpha/(width/(smooth*segments))
      local length = math.Clamp(velBar-(i*2), 0, velBar-(i*2))


      /*
      if i <= width/2 then
         alpha = Lerp(0.25, math.Clamp(rate*(i-1), 0, normalAlpha ), math.Clamp(rate*i, 0, normalAlpha ))
      else

        alpha = Lerp(0.25, math.Clamp(rate*(width-(i-1)), 0, normalAlpha ), math.Clamp(rate*(width-i), 0, normalAlpha ) )
      end*/


      surface.SetDrawColor( Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, rate) )
      surface.DrawRect( x+i  , y, length, height )
    end
    --surface.DrawRect( x , y, vel*maxVelWidth, height )


render.SetStencilReferenceValue( 11 )
    render.SetStencilWriteMask( 255  )
    render.SetStencilTestMask( 255 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
    render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
    render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilPassOperation( STENCILOPERATION_KEEP )

    render.SetBlend( 0 )

      surface.SetDrawColor( Color(255,0,0,255) )
      draw.NoTexture()
      surface.DrawRect( x, y,velBar , height )


    render.SetBlend( 1 )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_GREATER )


    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )
    local tx, ty = surface.GetTextSize( math.Round( vel ) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round( vel ) )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )


    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( hud.customColor2 )
    local tx, ty = surface.GetTextSize( math.Round( vel ) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round( vel ) )
  render.SetStencilEnable(false)
end
    surface.SetDrawColor( hud.customColor2 )
    --surface.DrawRect( x, y+height-3, width, 3 )
  /* --UNUSED
    -- The graph outline
    if outline then
      render.SetStencilReferenceValue( 10 )
      render.SetStencilFailOperation( STENCILOPERATION_KEEP )
      render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
      render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )

      render.SetBlend( 0 )
        surface.SetDrawColor( Color(0,0,0,1) )
        draw.NoTexture()
        surface.DrawPoly( SpeedoMeter.polyOutline )

        render.SetStencilPassOperation( STENCILOPERATION_ZERO )
        render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
        surface.SetDrawColor( Color(0,0,0,1) )
        draw.NoTexture()
        surface.DrawPoly( SpeedoMeter.Poly )

      render.SetBlend( 1 )
      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
      surface.SetDrawColor( outlineCol )
      surface.DrawRect( x+2, y, width-4, height )
    end*/
end

HPWave = {}
prevHP = 0
function healthDisplay:Draw3D(ply, w, h)
  if !hudtoggle:GetBool() then return end
  local height = 80              -- height of the panel
  local width  = 200             -- width of the panel
  local x      = -910            -- x position of the panel
  local y      = 310+height-10      -- y position of the panel
  local localPly = ply
  local scrollspeed = 4
  local outlineWidth = 2
  if ply:GetObserverTarget() then
    ply = ply:GetObserverTarget()
  end
  local hp    = Lerp(0.1, prevHP, ply:Health())
  prevHP = hp
  local fillCol = Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, 200 )      -- fill color



    surface.SetDrawColor( hud.color )
    surface.DrawRect( x, y, width, height )

        -- Stencil start
    render.ClearStencil()
    render.SetStencilEnable( true )
    render.SetStencilWriteMask( 255 )
    render.SetStencilTestMask( 255 )

    -- The graph fill
    render.SetStencilReferenceValue( 10 )


    render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilPassOperation( STENCILOPERATION_KEEP )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )
    render.SetStencilFailOperation( STENCILOPERATION_REPLACE )


    local normalAlpha = 120
    local segments = 1
    local smooth = 20
    local maxHPWidth = (width)/100
    local hpWidth    = math.Clamp(hp,0, 100)*maxHPWidth
    hpBar = hpWidth or Lerp(0.25, hpBar, hpWidth) and hpBar

    for i = 1, smooth, segments do
      local alpha = 0
      local rate = normalAlpha/(width/(smooth*segments))
      local length = math.Clamp(hpBar-(i*2), 0, hpBar-(i*2))

      surface.SetDrawColor( Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, rate) )
      surface.DrawRect( x+i  , y, length, height )
    end
    --surface.DrawRect( x , y, vel*maxVelWidth, height )


render.SetStencilReferenceValue( 11 )
    render.SetStencilWriteMask( 255  )
    render.SetStencilTestMask( 255 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
    render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
    render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilPassOperation( STENCILOPERATION_KEEP )

    render.SetBlend( 0 )

      surface.SetDrawColor( Color(255,0,0,255) )
      draw.NoTexture()
      surface.DrawRect( x, y,hpBar , height )


    render.SetBlend( 1 )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_GREATER )


    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )
    local tx, ty = surface.GetTextSize( math.Round(hp) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round(hp) )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )


    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( hud.customColor2 )
    local tx, ty = surface.GetTextSize( math.Round(hp) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round(hp) )
  render.SetStencilEnable(false)

end
--]=]
