util.AddNetworkString( "SendCache" )

/*
  Find key in table
*/
local function HasKey(table, key)
  for _, value in pairs(table) do
    if _ == key then
      return true
    end
  end
  return false
end

local Jump_Pattern = include( "watchlist.lua" )

/*
  Number of keystrokes and time between first and last keystroke that is probably a cheated
*/
Jump_Pattern.KeyThreshold = 20
Jump_Pattern.TimeThreshold = 0.68

Jump_Pattern.RemoveOnSuspect = false -- Remove players from watchlist once suspected
Jump_Pattern.RemoveOnTime = 0        -- How long in seconds the players should be watchlisted 0 for infinite
Jump_Pattern.Cache = {}              -- Storage
Jump_Pattern:Event("KeyPress")

function Jump_Pattern:PlayerAdded(ply)
  if Jump_Pattern.Cache[ply:SteamID()] then
    print("a")
    Jump_Pattern.players[ply:SteamID()] = Jump_Pattern.Cache[ply:SteamID()]
  else
    Jump_Pattern.players[ply:SteamID()] = {
      ["added"] = CurTime(),          -- Point of which the player was added
      ["player"] = ply,
      ["jumps"] = {},  -- table of jumps and intervals
      ["suspect"] = false,            -- Is suspected?
      ["bestTime"] = 999,             -- lowest jump interval after KeyThreshold reached
      ["TthresholdDist"] = 999,       -- How close to the time threshold
      ["JPS"] = {
        ["time"] = CurTime(),         -- Jumps per second started
        ["total"] = 0,
        ["avg"] = 0,                  -- Average Jumps per second
        ["jumps"] = 0
      },                    -- Jumps per second
      ["cache"] = {
        ["jumps"] = {},
        ["JPS"] = {}
      }
    }
  end

end
function Jump_Pattern:PlayerRemoved(ply)

  Jump_Pattern.Cache[ply:SteamID()] = Jump_Pattern.players[ply:SteamID()]
end

Jump_Pattern:Function(function( ply, key )
  /*
  if key == IN_SPEED then

    Jump_Pattern:AddPlayer(ply)
  end
*/
  if HasKey(Jump_Pattern.players, ply:SteamID()) and key == 2 then

    table.insert(Jump_Pattern.players[ply:SteamID()].jumps, CurTime())

    if Jump_Pattern.players[ply:SteamID()].JPS.time+1 > CurTime() then
      Jump_Pattern.players[ply:SteamID()].JPS.jumps = Jump_Pattern.players[ply:SteamID()].JPS.jumps+1
    else
      table.insert(Jump_Pattern.players[ply:SteamID()].cache.JPS, Jump_Pattern.players[ply:SteamID()].JPS.jumps)
      Jump_Pattern.players[ply:SteamID()].JPS.total = Jump_Pattern.players[ply:SteamID()].JPS.total+Jump_Pattern.players[ply:SteamID()].JPS.jumps
      Jump_Pattern.players[ply:SteamID()].JPS.jumps = 0
      Jump_Pattern.players[ply:SteamID()].JPS.time = CurTime()
      Jump_Pattern.players[ply:SteamID()].JPS.avg = math.Round(Jump_Pattern.players[ply:SteamID()].JPS.total / #Jump_Pattern.players[ply:SteamID()].cache.JPS)
    end
    -- Keythreshold reached
    if #Jump_Pattern.players[ply:SteamID()].jumps == Jump_Pattern.KeyThreshold then
      local time = Jump_Pattern.players[ply:SteamID()].jumps[#Jump_Pattern.players[ply:SteamID()].jumps] - Jump_Pattern.players[ply:SteamID()].jumps[1]

      -- Fastest jump time
      if time < Jump_Pattern.players[ply:SteamID()].bestTime then
        Jump_Pattern.players[ply:SteamID()].bestTime = time
        Jump_Pattern.players[ply:SteamID()].TthresholdDist = time - Jump_Pattern.TimeThreshold
      end
    end
    print(Jump_Pattern.players[ply:SteamID()].JPS.avg)
    -- Probably cheater
    if Jump_Pattern.players[ply:SteamID()].bestTime <= Jump_Pattern.TimeThreshold then

      Jump_Pattern.players[ply:SteamID()].suspect = true
    end

    -- Stop tracking and save the cache
    if Jump_Pattern.RemoveOnSuspect and Jump_Pattern.players[ply:SteamID()].suspect then
      Jump_Pattern:RemovePlayer(ply)
    end

    -- Times up
    if Jump_Pattern.RemoveOnTime+Jump_Pattern.players[ply:SteamID()].added <= CurTime() and Jump_Pattern.RemoveOnTime >= 1 then
      Jump_Pattern:PlayerRemoved(ply)
    end

  end

end)

hook.Add("OnPlayerHitGround", "WL Did Land", function(ply, inWater)
  if !Jump_Pattern.players[ply:SteamID()] then return end
  if Jump_Pattern.players[ply:SteamID()].jumps then
    table.Add(Jump_Pattern.players[ply:SteamID()].cache.jumps, Jump_Pattern.players[ply:SteamID()].jumps)
    table.Add(Jump_Pattern.players[ply:SteamID()].cache.jumps, {"ONGROUND"})
    Jump_Pattern.players[ply:SteamID()].jumps = {}
  end
end)

net.Receive("SendCache", function()
  local ply = net.ReadEntity()
  local target = net.ReadEntity()

  if target:IsWatchlisted() then
    table.Add(Jump_Pattern.players[target:SteamID()].cache.jumps, Jump_Pattern.players[target:SteamID()].jumps)
    Jump_Pattern.players[target:SteamID()].jumps = {}
    net.Start("SendCache")
      net.WriteTable(Jump_Pattern.players[ply:SteamID()])
      net.WriteFloat(Jump_Pattern.KeyThreshold)
      net.WriteFloat(Jump_Pattern.TimeThreshold)

    net.Send(ply)
  end
end)
