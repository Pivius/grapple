local PLAYER = FindMetaTable( "Player" )


if SERVER then
  util.AddNetworkString( "Settings_Grab" )
end

if CLIENT then
  function grabVar(string, ply)

    net.Start("Settings_Grab")
      net.WriteString(string)
      net.WriteEntity(ply)
      net.WriteEntity(LocalPlayer())
    net.SendToServer()
  end
  net.Receive("Settings_Grab", function()
    local func = net.ReadString()
    local result = net.ReadType()
    local target = net.ReadEntity()
    getmetatable(target)[func](target, result)
    --getmetatable(target)[func](result)
  end)
else
  net.Receive("Settings_Grab", function()
    local str = net.ReadString()
    local target = net.ReadEntity()
    local ply = net.ReadEntity()

    net.Start("Settings_Grab")
      net.WriteString(str)
      net.WriteType( target.Settings[str] )
      net.WriteEntity(target)
    net.Send(ply)
  end)
end


/*
  NAME - AddSettings
  FUNCTION - Get or set values
    GET: player:name()
    SET: player:name(var)
*/
function PLAYER:AddSettings(name, default)

  self.Settings[name] = default
  self.Default[name] = default

  local copy = getmetatable(name)
  local createFunc = {}
  createFunc  = {
  	[name] = function( self, var )
      if !var and !isbool(var) then

        return self.Settings[name]
      elseif var or isbool(var) then

        self.Settings[name] = var
      end
  	end
  }
  table.Merge(copy, createFunc)

	createFunc.__index = copy

  setmetatable( getmetatable(self), createFunc )
end

function PLAYER:createSettings()
  //Movement
  self:AddSettings("Accel", 10)         -- accelerate
  self:AddSettings("AirAccel", 10)      -- Air accelerate
  self:AddSettings("AutoHop", false)    -- AutoBhop
  self:AddSettings("Gain", 15)          -- Air Gain
  self:AddSettings("Friction", 8)       -- Friction
  self:AddSettings("Sliding", false)    -- Is sliding on ramp
  self:AddSettings("GroundTicks", 0)    -- Ticks spent on ground
  self:AddSettings("SlideNormal", 0.75) -- How steep a ramp can be before you're able to surf on it

  //Spectate
  self:AddSettings("CanSpec", true)     -- Determines if the player can spectate or not.

  //Lobby
  self:AddSettings("InLobby", false)

  //Stats
  self:AddSettings("TSS", 0) --Topspeed Session
  self:AddSettings("TSA", 0) --Topspeed AllTime

  //Grapple
  self:AddSettings("GrapplePos", Vector(0,0,0)) -- The Position of the grappling hook
  self:AddSettings("TracePos", Vector(0,0,0))   -- The grapple trace. Mostly required for my syncing
  self:AddSettings("AllowGrapple", true)        -- Decides if the player can grapple
  self:AddSettings("Grappling", false)          -- Is the player grappling or not
  self:AddSettings("Retracting", false)         -- Is the player retracting or not
  self:AddSettings("RetractSpeed", 150)         -- The speed the player retracts at
  self:AddSettings("RopeTension", 0)            -- Rope tension
  self:AddSettings("RopeLength", 0)             -- Rope length
  self:AddSettings("GrappleSync", {})           -- Sync table
  self:AddSettings("Intersections", {})        -- Intersecting rope
  //self:AddSettings("Extending", false)
  //self:AddSettings("ExtendSpeed", 75)

  //Utility
  self:AddSettings("Noclip", false)         -- Noclip
end

function PLAYER:Speed()
  return self:GetVelocity():Length()/10
end

function PLAYER:Speed2D()
  return self:GetVelocity():Length2D()/10
end

function PLAYER:SpeedZ()
  return math.abs(self:GetVelocity().z/10)
end

//
//  Hooks
//

hook.Add('PlayerTick', 'UpdateVel', function(ply, mv)

  if ply:Speed2D() > ply:TSS() then
    ply:TSS(ply:Speed2D())

  end
end)

hook.Add('PlayerDeathThink', 'Player_Death_Think', function(ply)
  if ( ply.NextSpawnTime && ply.NextSpawnTime > CurTime() ) then return end
  if ( ply:IsBot() || ply:KeyPressed( IN_ATTACK ) || ply:KeyPressed( IN_ATTACK2 ) || ply:KeyPressed( IN_JUMP ) ) then

  	ply:Spawn()

  end
end)
hook.Add("OnEntityCreated","Player_Init_Spawn",function(ply)
  local plMeta = getmetatable(ply)
	if plMeta!=FindMetaTable("Player") then
    return
  end
    ply.Default = {}
  ply.Settings = {}
  ply:createSettings()

  if SERVER then
    ply.MakeSettings = true

    ply:UnLock()

  	ply:TrailLife(275)
  	ply:TrailAdditive(true)
  	ply:TrailSize(4, 0)
  	ply:TrailRes(0.01)

  	ply:StartFade(2)
  	ply:TrailMat('trick/trails/beam_generic_white.vmt')--trick/test.vmt
  	--ply:TrailMat('trick/test.vmt')--trick/test.vmt
	   ply:SetCanWalk( false )
     if !ply.rope then
       ply.rope = ents.Create("rope")
       ply.rope:SetParent(ply)
       ply.rope:SetPos(ply:GetPos())
       ply.rope:Spawn()
     end
   end
end)

//Y NO WORK
/*
hook.Add('PlayerInitialSpawn', 'Player_Init_Spawn', function(ply)
  ply.MakeSettings = true

  ply:UnLock()
  ply:TrailColor(Color(255,255,255))
	ply:TrailLife(275)
	ply:TrailAdditive(true)
	ply:TrailSize(4, 0)
	ply:TrailRes(0.01)

	ply:StartFade(2)
	ply:TrailMat('trick/trails/beam_generic_white.vmt')--trick/test.vmt
	--ply:TrailMat('trick/test.vmt')--trick/test.vmt
	ply:SetCanWalk( false )

end)
*/
hook.Add('PlayerSpawn', 'Player_Spawn', function( ply )
  ply:CreateTrail()
end)

hook.Add('PlayerDeath', 'Player_Death', function ( ply )

	ply:RemoveTrail()
end)

hook.Add('PlayerDisconnected', 'Player_Disc', function( ply )
	ply:RemoveTrail()
end)

--Player Regeneration
function Regen()
  if CurTime() > (Lastthink or 0) + 0.6 then
    Lastthink = CurTime()
    for _,pl in pairs (player.GetAll()) do
      local hp = pl:Health()
      if hp < 100 then
        if (pl.LastHit or 0) + 5 < CurTime() and pl:Alive() then
          pl:SetHealth(hp+1)
        end
      end
      if hp < 0 then
        pl:SetHealth(0)
      end
    end
  end
end
hook.Add("Think", "Regeneration", Regen)
