include("my_sql.lua")
SQL = {}
SQLdata = SQLdata or {}
SQL.Use = true -- Set this to false if you don't want MySQL

local data = grapple.database

mysql:Disconnect()
if SQL.Use then
	mysql:Connect(data.Host, data.User, data.Pass, data.Database, data.Port)
end



hook.Add("DatabaseConnected", "Create Tables", function()

	local qadmins = mysql:Create(SQLDetails.tables.users)
		qadmins:Create("sID", "VARCHAR(255)")
    qadmins:Create("name", "VARCHAR(255)")
    qadmins:Create("rank", "VARCHAR(255)")
		qadmins:Create("flags", "VARCHAR(255)")
    qadmins:PrimaryKey("sID")
  qadmins:Execute()

	local qranks = mysql:Create(SQLDetails.tables.ranks)
		qranks:Create("name", "VARCHAR(255)")
		qranks:Create("inheritance", "VARCHAR(255)")
		qranks:Create("cmds", "VARCHAR(255)")
		qranks:Create("power", "TINYINT(255)")
		qranks:PrimaryKey("name")
	qranks:Execute()

	Admins:Init()
end)

timer.Create( "mysql.Timer", 1, 0, function()
    mysql:Think()
end )
