
include( 'shared.lua' )
include( 'sql/database.lua' )
include( 'sql/core.lua' )

AddCSLuaFile( 'shared.lua' )
AddCSLuaFile( 'modules/cl_hudmodule.lua' )
AddCSLuaFile( "player_class/player_sandbox.lua" )

DEFINE_BASECLASS( 'gamemode_base' )
GM.PlayerSpawnTime = {}

hook.Add('Initialize', 'console', function()
	game.ConsoleCommand('sv_maxvelocity 9999\n')
	game.ConsoleCommand( 'sv_friction 8\n' )
	game.ConsoleCommand( 'sv_gravity 400\n' )
	game.ConsoleCommand('sv_sticktoground 1\n')
	game.ConsoleCommand('sv_airaccelerate 10\n')
	game.ConsoleCommand('sv_accelerate 10\n')
	game.ConsoleCommand('mp_falldamage 0\n')
	/*
	GetSet('pkrPoints', 'Player')
	GetSet('inLobby', 'Player')
	GetSet('zoneOrder', 'Player')
	GetSet('zoneStatus', 'Player')
	GetSet('zoneSpeed', 'Player')
	GetSet('lastTrick', 'Player')
	GetSet('Points', 'Player')
	GetSet('trickRank', 'Player')
	GetSet('zoneTime', 'Player')
	GetSet('prevZone', 'Player')
	GetSet('Status', 'Player')
	*/
end)
Admin:Init()
--[[---------------------------------------------------------
   Name: gamemode:PlayerSpawn( )
   Desc: Called when a player spawns
-----------------------------------------------------------]]
function GM:PlayerSpawn( pl )


	player_manager.SetPlayerClass( pl, 'player_sandbox' )

	BaseClass.PlayerSpawn( self, pl )

	pl:SetAvoidPlayers( false )
	pl:SetCollisionGroup( COLLISION_GROUP_WEAPON )

end

-- Set the ServerName every 30 seconds in case it changes..
-- This is for backwards compatibility only - client can now use GetHostName()
local function HostnameThink()

	SetGlobalString( "ServerName", GetHostName() )

end
timer.Create( "HostnameThink", 30, 0, HostnameThink )

function GM:GetFallDamage( ply, speed )
	 return ( speed / 25)
end
