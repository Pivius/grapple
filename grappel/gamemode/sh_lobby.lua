lobby = lobby or {}

lobby.Minigames = {
	["Miscellaneous"]  = Color(155,255,255,255),
	["Hunted"]				 = Color(0,75,255,255),
	["Elimination"] 	 = Color(255,75,0,255),
	["Tag"]				 		 = Color(155,255,155,255),
  ["None"]           = Color(255,255,255,255)
}

function lobby:SetColor(ply, color, secondary)
	if SERVER then
		ply:TrailColor(color)
	  ply.rope:SetRopeColor(Vector(color.r,color.g,color.b))
		if !secondary then
			secondary = nil
		end
		net.Start("lobbyNetworking")
	  	net.WriteString("color")
	  	net.WriteTable( {ply, color, secondary} )
	  net.Send(ply)
	else
		hud.customColor = color
		if !secondary then
			secondary = Color(0,0,0)
		end
		hud.customColor2 = secondary
	end
end



function lobby:getPlayerLobby(ply)
  if SERVER then
    local lobby = ply:InLobby()
    if lobby == "none" then return "None" end
    return lobby.lobbies[lobby]
  else
    if lobby.lobbies == {} then return "None" end
    for k, v in pairs(lobby.lobbies) do
      if table.HasValue(v["lobbyPlayers"], ply) then
        return lobby.lobbies[k]
      end
    end
  end
  return "None"
end

net.Receive( "lobbyNetworking", function()
 local type = net.ReadString()
 local varargs = net.ReadTable()
 if type == "sync" then
	 if SERVER then
     local ply = varargs[1]
		 if ply:IsValid() then
	     net.Start("lobbyNetworking")
	       net.WriteString("sync")
	       net.WriteTable( {[1] = lobby:getLobbies( )} )
	     net.Send(ply)
		 end
   else

    lobby.lobbies = varargs[1]
   end
 elseif type == "create" then
	 if SERVER then
		 local ply = varargs[1]
		 local mg = varargs[2]
		 local var = varargs[3]
		 if ply:IsValid() then
		 	lobby:openLobby(ply, mg, var)
		end
	 end
 elseif type == "join" then
	 if SERVER then
		 local ply = varargs[1]
		 local lbID = varargs[2]
		 local spectator = varargs[3]
		 if lobby:isValidLobby( lbID ) and ply:IsValid() then
			 lobby:addPlayerLobby(lbID, ply)
		 end
	 end
 elseif type == "leave" then
	 if SERVER then
		 local ply = varargs[1]
		 local LobbyID = ply:InLobby()
		 if lobby:isValidLobby( LobbyID ) and ply:IsValid() then

			 lobby:removePlayerLobby(LobbyID, ply)
		 end
	 end
	elseif type == "color" then
		if CLIENT then
			local ply = varargs[1]
			local color = varargs[2]
			local secondary = varargs[3]
			lobby:SetColor(ply, color, secondary)
		end
	end
end )
