Admin.Commands:createCMD("noclip", "caller ; target", function(caller, target)

	if target then
    if !Admin:IsHigherThan( caller, Admin.Player:GetRank(target:SteamID()) ) then
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You can't target this user!")
      return
    end

    if target:Noclip() then
      target:Noclip(false)
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "You have disabled ", Admin.Colors["VGAM"], target:Nick(), Admin.Colors["normal"], "'s noclip.")
      chat.Text(target, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Noclip off.")
    else
      target:Noclip(true)
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "You have enabled ", Admin.Colors["VGAM"], target:Nick(), Admin.Colors["normal"], "'s noclip.")
			chat.Text(target, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Noclip on.")
    end

  else
    if caller:Noclip() then
      caller:Noclip(false)
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Noclip off.")
    else
      caller:Noclip(true)
			chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Noclip on.")
    end
	end
end)
