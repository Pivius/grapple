include('shared.lua')


function ENT:getTrailNode(n)
  if self.nodes[n] == nil then return nil end
  return self.nodes[n]
end

-- Scale renderbound with trail.
function ENT:updateBoundingBox()
  local renderOrigin = self:GetPos()
	local renderMins = renderOrigin
	local renderMaxs = renderOrigin
	local maxs, mins = Vector()

  for i=1, #self.nodes do
    local tNode = self:getTrailNode(i)
    if tNode == nil then return end

    -- Node width
    local nWidth = Vector(tNode.Width,tNode.Width,tNode.Width)
    mins = tNode.Pos - nWidth
    maxs = tNode.Pos + nWidth

    -- Gets the vector that is the furthest out.
    renderMins = Vector(math.min(renderMins.x, mins.x),math.min(renderMins.y, mins.y),math.min(renderMins.z, mins.z))
    renderMaxs = Vector(math.max(renderMaxs.x, maxs.x),math.max(renderMaxs.y, maxs.y),math.max(renderMaxs.z, maxs.z))
  end

  -- Get localvector
	renderMins = renderMins - renderOrigin
  renderMaxs = renderMaxs - renderOrigin

  self:SetRenderBounds(renderMins, renderMaxs)
end


function ENT:updateTrails()
  local lifetime   = self:GetLifeTime()
  local endWidth   = self:GetEndWidth()
  local startWidth = self:GetStartWidth()
  local startFade  = self:GetStartFade()*100 -- To avoid values in the 1000s
  local Dashed     = self:GetDashed()

  if startFade == 0 then
    startFade = 1
  end

  for i=1, #self.nodes do
    local tNode = self:getTrailNode(i)
    if tNode == nil then return end

    local dieTime = ( CurTime( ) - tNode.Time ) / (lifetime)
    dieTime       = math.Clamp( dieTime , 0, 1 )

    local liveTime = ( CurTime( ) - tNode.Time ) / (startFade/lifetime)
    liveTime       = math.Clamp( liveTime , 0, 1 )

    if tNode.Alpha >= 254 then

      tNode.fadeOut = true
    end

    -- Check if the node can fade out
    if tNode.fadeOut == false then
      tNode.Alpha = Lerp( liveTime, tNode.Alpha, self:GetTrailAlpha() )
    else
      if lifetime != 0 then
        tNode.Alpha = Lerp(dieTime, tNode.Alpha, 0 )
      end
    end

    if Dashed then -- Dashed trail
      if !tNode.Dashed then
        tNode.Alpha = 0 -- Sets the alpha of segments in between dashes
      end
    end

    if i > 1 then
      -- Checks to see if the distance between each nodes is too big
      if tNode.Pos:Distance( self:getTrailNode(i-1).Pos) > self:GetNodeLength()+300 then
        --Set the alpha to 0
        tNode.Alpha = 0
        self:getTrailNode(i-1).Alpha = 0
      end
    end

    -- Doesn't fade out trail if lifetime is 0
    if lifetime != 0 then
      tNode.Width = Lerp( dieTime/2, tNode.Width, endWidth )
    end

    -- Node width can't be greater than starwidth
    if tNode.Width > startWidth then
      tNode.Width = startWidth
    end

    -- Remove unnecessary nodes
    if endWidth < startWidth then
      if math.Round( tNode.Width, 2 ) <= endWidth or math.Round( tNode.Width, 2 ) <= 0.25  then
        table.remove( self.nodes, i )
      end
    else
      if math.Round( tNode.Width, 1 ) >= endWidth then
        table.remove( self.nodes, i )
      end
    end
  end

  if self:GetReset() then
    self:SetReset(false)
    self.nodes = {}
    self.pLast = 0
  end
end

function ENT:Initialize()
  self:SetRenderAngles( Angle(0,0,0) )
  self.pLast = 0 -- Last node
  self.nodes = {}
  self.wasDashed = false
end

function ENT:createNode()
  self.pLast = #self.nodes -- Amount of nodes / last node

  local lastNode = self.nodes[self.pLast]
  local newPoint = {}
  newPoint.Width = self:GetStartWidth() -- Point start width
  newPoint.Time  = CurTime() -- Time of creation
  newPoint.Pos   = self:GetPos()+Vector(0,0,5) -- Position of the trail. +5 in Z vector to avoid clipping in ground
  newPoint.Color = Color( self:GetTrailColor().x, self:GetTrailColor().y, self:GetTrailColor().z, self:GetTrailAlpha() )
  newPoint.Solid = true -- Is Solid
  newPoint.Dashed = false
  newPoint.wasDashed = self.wasDashed
  self.wasDashed = self:GetDashed()
  if self:GetStartFade() == 0 then -- No start fade?
    newPoint.Alpha = self:GetTrailAlpha()
    newPoint.fadeOut = true
  else
    newPoint.Alpha = 0
    newPoint.fadeOut = false
  end

  ---------- Dashed trail ----------
  if self:GetDashed() then -- Is Dashed?
    newPoint.Solid = false
    if lastNode == nil then -- Last node gone?
      newPoint.Dashed = true
      table.insert( self.nodes, newPoint )
    else
      local prevDashed = lastNode.Dashed -- Was previous node dashed?

      local Distance =  math.Round((lastNode.Pos - newPoint.Pos):Length()) -- New node and old node distance

      if prevDashed then

        if Distance >= self:GetNodeLength() then -- Invisible dash
          local D = (lastNode.Pos - newPoint.Pos) -- Position correction incase of distance overshooting
          D:Normalize()
          D:Mul(math.Round(Distance-self:GetNodeLength()))

          local t         = newPoint.Pos + D
          newPoint.Pos    = t
          newPoint.Dashed = false

          table.insert( self.nodes, newPoint )
        end
      else
        if Distance >= self:GetDashDist() then -- Visible dash
          local D = (lastNode.Pos - newPoint.Pos) -- Position correction incase of distance overshooting
          D:Normalize()
          D:Mul(math.Round(Distance-self:GetDashDist()))

          local t = newPoint.Pos + D
          newPoint.Pos = t
          newPoint.Dashed = true

          table.insert( self.nodes, newPoint )
        end
      end
    end
  else
    --———————— Solid trail ——————————
    if newPoint.wasDashed then
      local nP = {}
      nP.Width = self:GetStartWidth() -- Point start width
      nP.Time  = CurTime() -- Time of creation
      nP.Pos   = self:GetPos()+Vector(0,0,5) -- Position of the trail. +5 in Z vector to avoid clipping in ground
      nP.Color = Color( self:GetTrailColor().x, self:GetTrailColor().y, self:GetTrailColor().z, 0 )
      nP.Solid = true -- Is Solid
      nP.Dashed = false
      nP.wasDashed = false
      nP.Alpha = 0

      self:getTrailNode(#self.nodes).Alpha = 0
      table.insert( self.nodes, nP )
    end
    if lastNode == nil then
      table.insert( self.nodes, newPoint )
    else
      -- Avoid making nodes shorter than nodelength.
      if lastNode.Pos:Distance( newPoint.Pos ) > self:GetNodeLength() then
        table.insert( self.nodes, newPoint )
      end
    end
  end
end

function ENT:Think()
  -- If you can't see the trail at some angles even though it should be visible,
  -- try enabling ENT:UpdateTransmitState() serverside and remove self:updateBoundingBox().
  self:updateBoundingBox() -- Update boundingbox to make it visible when looking at it.
  self:updateTrails() -- Update trail alpha and what not.
  if IsValid( self:GetParent() ) then -- Check to see if this node has a parent.
    if self:GetParent():GetVelocity():Length() == 0 and !self:GetDashed() then -- Avoid unnecessary nodes.

      return
    end
  else -- Do not need to update anything if it's not parented.

    return
  end
  self:createNode() -- Create new node.

end

-- Draw trail
function DrawTrail(ent)
  -- Variables for the segment under the player.
  local width = ent:GetStartWidth() -- Trail width.
  local color = Color(255,255,255,0) -- Trail color.
  if ent:getTrailNode(#ent.nodes) != nil then
    width = ent:getTrailNode(#ent.nodes).Width -- Trail width.
    color = ent:getTrailNode(#ent.nodes).Color -- Trail Color.
  end

  render.SetMaterial( Material( ent:GetMaterial() ) )

  ---------- Dashed trail ----------
  for k, v in pairs( ent.nodes ) do
    if v.Dashed and !v.Solid then -- Is Dashed
      if k+1 <= #ent.nodes then
        render.StartBeam( 2 )
          render.AddBeam( v.Pos , v.Width, 1.0, Color( v.Color.r, v.Color.g, v.Color.b, v.Alpha ) )
          render.AddBeam( ent:getTrailNode(k+1).Pos , ent:getTrailNode(k+1).Width, 1.0, Color( v.Color.r, v.Color.g, v.Color.b, v.Alpha ) )
        render.EndBeam()
      end
    elseif !v.Dashed and !v.Solid then
      if k+1 <= #ent.nodes then
        render.StartBeam( 2 )
          render.AddBeam( v.Pos , v.Width, 1.0, Color( v.Color.r, v.Color.g, v.Color.b, v.Alpha ) )
          render.AddBeam( ent:getTrailNode(k+1).Pos , ent:getTrailNode(k+1).Width, 1.0, Color( v.Color.r, v.Color.g, v.Color.b, v.Alpha ) )
        render.EndBeam()
      end
    end
  end

  if !ent:getTrailNode(#ent.nodes).Solid then
    render.StartBeam(2) -- The line segment under the player
      if ent:GetStartFade() == 0  then -- Should not fade in?
        local lastNode = ent:getTrailNode(#ent.nodes)
        render.AddBeam( lastNode.Pos, lastNode.Width, 1.0, Color( lastNode.Color.r, lastNode.Color.g, lastNode.Color.b, lastNode.Alpha ) )
        render.AddBeam( ent:GetPos() + Vector( 0, 0, 5 ), width, 1.0, Color( color.r, color.g, color.b, ent:GetTrailAlpha() ) )
      else -- Should fade in
        local lastNode = ent:getTrailNode(#ent.nodes)
        if (lastNode and lastNode.Dashed) then
          render.AddBeam( lastNode.Pos, lastNode.Width, 1.0, Color( lastNode.Color.r, lastNode.Color.g, lastNode.Color.b, lastNode.Alpha ) )
          render.AddBeam( ent:GetPos() + Vector( 0, 0, 5 ), width, 1.0, Color( lastNode.Color.r, lastNode.Color.g, lastNode.Color.b, lastNode.Alpha ) )
        end
      end
    render.EndBeam()
  end
  --———————— Solid trail ——————————

  render.StartBeam( #ent.nodes + 1 )
  for k, v in pairs( ent.nodes ) do
    if v.Solid then
      render.AddBeam( v.Pos , v.Width, 1.0, Color( v.Color.r, v.Color.g, v.Color.b, v.Alpha ) )
    end
  end
  if ent:getTrailNode(#ent.nodes).Solid then
    if ent:GetStartFade() == 0 then -- Show the trail beneath the player if it doesn't have startfade
      render.AddBeam( ent:GetPos() + Vector( 0, 0, 5 ), width, 1.0, Color( color.r, color.g, color.b, ent:getTrailNode(#ent.nodes).Alpha ) )
    else -- Invisible node, Exists only to get the fade effect on the last node.
      render.AddBeam( ent:GetPos()+Vector(0,0,5), width, 1.0, Color( color.r, color.g, color.b, 0 ) )
    end
  end
  render.EndBeam()
end

function ENT:Draw()
  if #self.nodes > 0 then
    DrawTrail(self)
  end
end
